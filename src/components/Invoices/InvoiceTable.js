import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Panel,
  ListGroup,
  ListGroupItem,
  Label,
  ButtonGroup,
  Button
} from 'react-bootstrap';
import ReactLoading from 'react-loading';
import Moment from 'react-moment';
import 'moment/locale/id';
import { load as loadInvoice, isLoaded, loadMore } from 'redux/modules/invoice';

@connect(state => ({
  user: state.auth.user,
  invoice: state.invoice
}), { loadInvoice, loadMore, isLoaded })

export default class InvoiceTable extends Component {
  componentDidMount() {
    const {
      loadInvoice, isLoaded, user, invoice
    } = this.props;

    if (!invoice.loaded) { loadInvoice(user.token); }
  }

  handleLoadMore() {
    const { loadMore, invoice, user } = this.props;
    loadMore(invoice, user.token);
  }

  render() {
    const { invoice, loadMore } = this.props;
    const styles = require('./Invoices.scss');
    return (
      <div>
        {invoice.loaded && (
          <div>
            <table className="table table-borderless table-striped">
              <thead>
                <tr className={styles.table_header}>
                  <td className="hidden-xs">Nomor Tagihan</td>
                  <td>Tanggal Terbit</td>
                  <td>Tanggal Jatuh Tempo</td>
                  <td className="hidden-xs">Harga</td>
                  <td>Status</td>
                  <td />
                </tr>
              </thead>
              <tbody>
                {invoice.data.map(invoiceDetail => (
                  <tr key={invoiceDetail.id}>
                    <td className="hidden-xs">
                      <Link to={`/invoices/${invoiceDetail.id}`}>{invoiceDetail.invoice_number}</Link>
                    </td>
                    <td>
                      <Moment
                        locale="id"
                        format="D MMMM YYYY"
                      >
                        {invoiceDetail.created_at}
                      </Moment>
                    </td>
                    <td>
                      <Moment
                        locale="id"
                        format="D MMMM YYYY"
                      >
                        {invoiceDetail.due_date}
                      </Moment>
                    </td>
                    <td className="hidden-xs">Rp {invoiceDetail.amount.fmt}</td>
                    <td><Label bsStyle={invoiceDetail.payment_status.style}>{invoiceDetail.payment_status.text}</Label></td>
                    <td><Link to={`/invoices/${invoiceDetail.id}`} className={`${styles.btn_showinvoice} btn btn-sm btn-block`}>Lihat</Link></td>
                  </tr>
                ))}
              </tbody>
            </table>
            {invoice.next_page_url && !invoice.loading && (
              <Button onClick={this.handleLoadMore.bind(this)} className={`${styles.btn_loadmore} btn-block btn`}>Muat Selengkapnya</Button>
            )}
          </div>
        )}
        {invoice.loading && (
          <ReactLoading type="bars" color="red" delay={0} className={styles.loading_container} />
        )}
      </div>
    );
  }
}
