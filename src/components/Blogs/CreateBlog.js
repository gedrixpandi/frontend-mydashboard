import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Panel,
  Button,
  FormGroup,
  InputGroup,
  FormControl,
  Alert
} from 'react-bootstrap';
import ReactLoading from 'react-loading';
import { checkDomain, resetData as resetDomainData } from 'redux/modules/domainAvailability';
import { create as createBlog, resetData as resetBlogData, load as loadBlog } from 'redux/modules/blog';
import { load as loadUser } from 'redux/modules/auth';
import { isDomainName } from 'utils/validation2';

@connect(state => ({
  domainAvailability: state.domainAvailability,
  user: state.auth.user,
  blog: state.blog,
  subscription: state.subscription
}), {
  checkDomain,
  resetDomainData,
  createBlog,
  resetBlogData,
  loadBlog,
  loadUser
})

export default class CreateBlog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      domain: null,
      is_available: false,
      is_checked: false,
      title: null,
      selected_domain: null,
      step: 1
    };
  }

  componentDidMount() {
    const { resetDomainData, resetBlogData, blog } = this.props;
    if (!blog.create_loading) {
      resetDomainData();
      resetBlogData();
    }
  }

  handleDomainChange(e) {
    this.setState({ domain: e.target.value });
  }

  handleTitleChange(e) {
    this.setState({ title: e.target.value });
  }

  handleDomainCheck() {
    const { checkDomain, user } = this.props;
    checkDomain(user.token, `${this.state.domain}.my.id`);
  }

  changeDomain() {
    const { resetDomainData } = this.props;
    resetDomainData();
    this.setState({ domain: null });
  }

  async createNewBlog() {
    const {
      createBlog, domainAvailability, resetDomainData, user, loadBlog, loadUser
    } = this.props;

    await createBlog({
      domain: domainAvailability.name,
      title: this.state.title
    }, user.token);

    loadBlog(user.token);
    resetDomainData();
    loadUser(user.token);
  }

  render() {
    const {
      domainAvailability, blog, user, subscription
    } = this.props;
    const styles = require('./Blogs.scss');
    const isValidDomain = isDomainName(this.state.domain);
    return (
      <Panel className={styles.panel}>
        <Panel.Heading className={styles.header}>Buat Blog Baru</Panel.Heading>
        <Panel.Body>
          {domainAvailability.checked && !blog.create_loading && !blog.create_loaded && (
            <Alert bsStyle={domainAvailability.is_available ? 'success' : 'danger'}>
              {domainAvailability.is_available ? (
                <div>
                  <strong>Selamat!</strong> Domain {domainAvailability.name} tersedia.
                </div>
              ) : (
                <div>
                  <strong>Ups!</strong> Domain {domainAvailability.name} tidak tersedia. ☹️<br />
                  Silakan coba nama domain yang lain.
                </div>
              )}
            </Alert>
          )}
          {!domainAvailability.is_available && !blog.create_loading && !blog.create_loaded && (subscription.data.status === 'Premium' || user.blog_count < 1) && (
            <div>
              <FormGroup validationState={domainAvailability.checked ? (domainAvailability.is_available ? 'success' : 'error') : null}>
                <p className={styles.form_label}>Masukkan nama domain yang kamu mau</p>
                {(this.state.domain && !isValidDomain) ? (
                  <center>
                    <span className={styles.invalid_domain}>Nama domain tidak sesuai!</span>
                  </center>
                ) : (
                  <center>
                    <span className={styles.invalid_domain}>&nbsp;</span>
                  </center>
                )}
                <InputGroup className={styles.input_domain_container}>
                  <FormControl className={styles.my_field} type="text" placeholder="Contoh: budi" onChange={this.handleDomainChange.bind(this)} />
                  <InputGroup.Addon className={styles.my_field_addon}>.my.id</InputGroup.Addon>
                </InputGroup>
              </FormGroup>
              <center>
                <Button className="btn btn-warning" onClick={this.handleDomainCheck.bind(this)} disabled={!this.state.domain || domainAvailability.loading || !isValidDomain}>
                  {domainAvailability.loading ? 'Mohon Tunggu...' : 'Periksa Ketersediaan Domain'}
                </Button>
              </center>
            </div>
          )}

          {domainAvailability.checked && domainAvailability.is_available && !blog.create_loading && !blog.create_loaded && (
            <FormGroup>
              <p className={styles.form_label}>Masukkan judul blog untuk {domainAvailability.name}</p>
              <InputGroup className={styles.input_domain_container}>
                <FormControl className={styles.my_field} type="text" placeholder="Contoh: Blog Budi" onChange={this.handleTitleChange.bind(this)} />
              </InputGroup>
              <div className={styles.control_container}>
                <Button onClick={this.changeDomain.bind(this)} className={styles.btn_cancel}>Batal</Button>
                <Button onClick={this.createNewBlog.bind(this)} className="btn btn-success">Buat Blog <i className="fa fa-angle-right" /></Button>
              </div>
            </FormGroup>
          )}

          {blog.create_loading && (
            <div className={styles.creating_blog}>
              <ReactLoading type="bars" color="red" delay={0} className={styles.loading_container} />
              Kami sedang menyiapkan blog baru milikmu. Mohon tunggu beberapa saat...
            </div>
          )}

          {blog.create_loaded && (
            <Alert bsStyle="success">
              <strong>Selamat!</strong> Blog kamu {domainAvailability.name} berhasil dibuat. Tunggu beberapa saat sampai propagasi domain selesai dan blogmu siap digunakan.
            </Alert>
          )}

          {subscription.data.status != 'Premium' && (user.blog_count > 0) && (
            <Panel>
              <Panel.Body>
                <center><h4>Kamu sudah memiliki blog.</h4></center>
                <center><p>Langganan blog premium untuk miliki blog lagi!</p></center>
                <Link className={`${styles.btn_newblog} btn btn-block`} to="/blogs/new-premium">Daftarkan Blog Premium Sekarang!</Link>
              </Panel.Body>
            </Panel>
          )}
        </Panel.Body>
      </Panel>
    );
  }
}
