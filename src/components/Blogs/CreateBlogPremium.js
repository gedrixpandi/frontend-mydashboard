import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Panel,
  Button,
  FormGroup,
  Alert
} from 'react-bootstrap';
import ReactLoading from 'react-loading';
import { checkDomain, resetData as resetDomainData } from 'redux/modules/domainAvailability';
import { load as loadOrder, addItem as addOrderItem } from 'redux/modules/order';
import { loadUidProfile } from 'redux/modules/profile';
import {
  create as createBlog,
  resetData as resetBlogData,
  load as loadBlog,
  loadDomainProducts
} from 'redux/modules/blog';
import { push } from 'react-router-redux';
import { load as loadUser } from 'redux/modules/auth';
import { isDomainName } from 'utils/validation2';

@connect(state => ({
  domainAvailability: state.domainAvailability,
  user: state.auth.user,
  blog: state.blog,
  subscription: state.subscription,
  domainProducts: state.blog.domainProducts,
  uid_profile: state.profile.uid_profile.data
}), {
  checkDomain,
  resetDomainData,
  createBlog,
  resetBlogData,
  loadBlog,
  loadUser,
  loadDomainProducts,
  loadUidProfile,
  pushState: push,
  addOrderItem,
  loadOrder
})

export default class CreateBlogPremium extends Component {
  constructor(props) {
    super(props);

    this.state = {
      domain: '',
      is_available: false,
      is_checked: false,
      title: null,
      selected_domain: null,
      step: 1,
      selectedDomainProduct: {},
      contactData: {}
    };

    this.updateSelectedDomain = this.updateSelectedDomain.bind(this);
    this.handleDomainChange = this.handleDomainChange.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleDomainCheck = this.handleDomainCheck.bind(this);
    this.handleAddToCart = this.handleAddToCart.bind(this);
  }

  componentDidMount() {
    const {
      resetDomainData, resetBlogData, blog
    } = this.props;
    if (!blog.create_loading) {
      resetDomainData();
      resetBlogData();
    }
    this.handleLoadDomainProducts();
    this.handleLoadUidProfile();
  }

  async handleLoadDomainProducts() {
    const { loadDomainProducts } = this.props;
    await loadDomainProducts();
    this.setState({
      selectedDomainProduct: this.props.domainProducts.data[0]
    });
  }

  async handleLoadUidProfile() {
    const { loadUidProfile, user } = this.props;
    await loadUidProfile(user.token);
    const { uid_profile } = this.props;
    this.setState({
      contactData: {
        name: uid_profile.full_name,
        state: uid_profile.address_ktp.province,
        city: uid_profile.address_ktp.city,
        address: uid_profile.address_ktp.road,
        email: uid_profile.email,
        phone: uid_profile.phone.replace(/\D/g, '')
      }
    });
  }

  handleDomainChange(e) {
    this.setState({ domain: e.target.value.toLowerCase() });
  }

  handleTitleChange(e) {
    this.setState({ title: e.target.value });
  }

  handleDomainCheck() {
    const { checkDomain, user } = this.props;
    checkDomain(user.token, `${this.state.domain}${this.state.selectedDomainProduct.name}`);
  }

  changeDomain() {
    const { resetDomainData } = this.props;
    resetDomainData();
    this.setState({ domain: null });
  }

  async createNewBlog() {
    const {
      createBlog, domainAvailability, resetDomainData, user, loadBlog, loadUser
    } = this.props;

    await createBlog({
      domain: domainAvailability.name,
      title: this.state.title
    }, user.token);

    loadBlog(user.token);
    resetDomainData();
    loadUser(user.token);
  }

  updateSelectedDomain(e) {
    const { domainProducts } = this.props;
    this.setState({
      selectedDomainProduct: domainProducts.data[e.target.value]
    });
  }

  async handleAddToCart() {
    const { selectedDomainProduct, contactData } = this.state;
    const { addOrderItem, user, pushState } = this.props;
    const preparedData = {
      product_id: selectedDomainProduct.id,
      domain_name: this.state.domain + selectedDomainProduct.name,
      name: contactData.name,
      state: contactData.state,
      city: contactData.city,
      email: contactData.email,
      phone: contactData.phone,
      address: contactData.address,
      period: 1,
      auto_renew: true,
      nameservers: [
        'ns1.domain.id',
        'ns2.domain.id'
      ],
      title: this.state.title,
      order_type: 'domain_bundling_new'
    };
    await addOrderItem(preparedData, user.token);
    pushState('/cart');
  }

  render() {
    const {
      domainAvailability, blog, user, subscription, domainProducts
    } = this.props;
    const styles = require('./Blogs.scss');
    const isValidDomain = isDomainName(this.state.domain) && this.state.domain.length > 4;
    return (
      <Panel className={styles.panel}>
        <Panel.Heading className={styles.header}>Buat Blog Baru</Panel.Heading>
        <Panel.Body>
          {domainAvailability.checked && !blog.create_loading && !blog.create_loaded && (
            <Alert bsStyle={domainAvailability.is_available ? 'success' : 'danger'}>
              {domainAvailability.is_available ? (
                <div>
                  <strong>Selamat!</strong> Domain {domainAvailability.name} tersedia.
                </div>
              ) : (
                <div>
                  <strong>Ups!</strong> Domain {domainAvailability.name} tidak tersedia. ☹️<br />
                  Silakan coba nama domain yang lain.
                </div>
              )}
            </Alert>
          )}
          <div>
            <FormGroup className={styles.form_wrapper} validationState={domainAvailability.checked ? (domainAvailability.is_available ? 'success' : 'error') : null}>
              {(this.state.domain && !isValidDomain) ? (
                <center>
                  <span className={styles.invalid_domain}>Nama domain tidak sesuai/kurang dari 5 karakter!</span>
                </center>
              ) : (
                <center>
                  <span className={styles.invalid_domain}>&nbsp;</span>
                </center>
              )}
              {(!this.state.domain || !domainAvailability.checked || !domainAvailability.is_available) && (
                <div>
                  <div className={styles.input_wrapper}>
                    <p>Pilih domain yang kamu mau:</p>
                    <select disabled={!domainProducts.loaded} onChange={this.updateSelectedDomain}>
                      {domainProducts.loaded && domainProducts.data.map((product, index) => (
                        <option key={product.id} value={index}>
                          {product.price.new.number > 0 ?
                            `${product.name} (Rp${product.price.new.fmt}/tahun)`
                            :
                            `${product.name} (Gratis)`
                          }
                        </option>
                      ))}
                    </select>
                  </div>
                  <div className={styles.input_wrapper}>
                    <p>Masukkan nama domain untuk blogmu:</p>
                    <div className={styles.domain_name_wrapper}>
                      <input placeholder="namadomain" onChange={this.handleDomainChange} />
                      <span>{this.state.selectedDomainProduct.name}</span>
                      {this.state.domain && isValidDomain && !domainAvailability.is_available && (
                        <button
                          className={styles.domain_check_button}
                          onClick={this.handleDomainCheck}
                          disabled={!this.state.domain || domainAvailability.loading || !isValidDomain}
                        >
                          {domainAvailability.loading ? 'Mohon Tunggu...' : 'Periksa Ketersediaan'}
                        </button>
                      )}
                    </div>
                  </div>
                </div>
              )}
              {this.state.domain && domainAvailability.checked && domainAvailability.is_available && (
                <div>
                  <p className={styles.current_domain_name}>{this.state.domain}{this.state.selectedDomainProduct.name}</p>
                  <div className={styles.input_wrapper}>
                    <p>Masukkan judul blogmu:</p>
                    <input placeholder="Blog Budi" onChange={this.handleTitleChange} />
                  </div>
                  <center>
                    <Button
                      className="btn btn-success"
                      onClick={this.handleAddToCart}
                      disabled={
                        !this.state.domain ||
                        domainAvailability.loading ||
                        !isValidDomain ||
                        !this.state.title ||
                        !domainAvailability.is_available
                      }
                    >
                      {domainAvailability.loading ?
                        'Mohon Tunggu...' :
                        (<span><i className="fa fa-shopping-cart" /> Masukkan Keranjang</span>)}
                    </Button>
                  </center>
                </div>
              )}
            </FormGroup>
          </div>
          {blog.create_loading && (
            <div className={styles.creating_blog}>
              <ReactLoading type="bars" color="red" delay={0} className={styles.loading_container} />
              Kami sedang menyiapkan blog baru milikmu. Mohon tunggu beberapa saat...
            </div>
          )}

          {blog.create_loaded && (
            <Alert bsStyle="success">
              <strong>Selamat!</strong> Blog kamu {domainAvailability.name} berhasil dibuat. Tunggu beberapa saat sampai propagasi domain selesai dan blogmu siap digunakan.
            </Alert>
          )}

          {subscription.data.status != 'Premium' && (user.blog_count > 100) && (
            <Panel>
              <Panel.Body>
                <center><h4>Ups, Kamu sudah memiliki blog.</h4></center>
                <center><p>Kami sedang menyiapkan fitur premium untuk kamu yang ingin memiliki blog lebih dari satu.<br />Nantikan kehadirannya dalam waktu dekat, dan kamu akan jadi yang pertama kami kabarkan!</p></center>
                <Link className={`${styles.btn_newblog} btn btn-block`} to="/">Kembali</Link>
              </Panel.Body>
            </Panel>
          )}
        </Panel.Body>
      </Panel>
    );
  }
}
