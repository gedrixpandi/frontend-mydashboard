import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Panel,
  ListGroup,
  ListGroupItem,
  Label
} from 'react-bootstrap';
import ReactLoading from 'react-loading';
import { isLoaded as isBlogLoaded, load as loadBlog } from 'redux/modules/blog';
import Countdown from 'react-countdown-now';
import moment from 'moment';

@connect(state => ({
  blogs: state.blog,
  user: state.auth.user,
  subscription: state.subscription
}), { isBlogLoaded, loadBlog })

export default class MyBlogsSlider extends Component {
  componentDidMount() {
    const {
      user, blogs, isBlogLoaded, loadBlog
    } = this.props;
    if (!blogs.loaded || blogs.create_loaded) {
      loadBlog(user.token);
    }
  }

  reloadBlog() {
    const { user, loadBlog } = this.props;
    loadBlog(user.token);
  }

  render() {
    const {
      blogs, user, subscription, withCreateButton, loadBlog
    } = this.props;
    const styles = require('./Blogs.scss');
    return (
      <Panel className={styles.panel}>
        <Panel.Heading className={styles.header}>Blog Saya</Panel.Heading>
        <Panel.Body>
          {blogs.loaded ? (
            <div className={styles.blogsWrapper}>
              {blogs.data.map(blog => (
                <Panel className={styles.blogCard}>
                  <Panel.Body className={styles.blogBody}>
                    <div className={styles.status_container}>
                      <Label bsStyle={blog.status.style}>{blog.status.text}</Label>
                    </div>
                    <div className="row">
                      <div className="col-lg-12">
                        <h5 className={styles.blog_title}>{blog.blogname}</h5>
                        <a className={styles.blog_domain} href={`http://${blog.alias || blog.domain}`} target="_blank">{blog.alias || blog.domain}</a>
                      </div>
                      <div className="col-lg-6 col-sm-6 col-xs-6">
                        {(blog.status.code !== 0 && blog.status.code !== 1) && (
                          <Link to={`blogs/${blog.domain}`} className="btn btn-default btn-xs"><i className="fa fa-cog" /> Atur Blog</Link>
                        )}
                        {blog.status.code === 0 && (
                          <Countdown
                            daysInHours
                            date={moment(blog.registered).add(15, 'm').toDate()}
                            onComplete={this.reloadBlog.bind(this)}
                          />
                        )}
                      </div>
                    </div>
                  </Panel.Body>
                </Panel>
              ))}
            </div>
          ) : (
            <ReactLoading type="bars" color="red" delay={0} className={styles.loading_container} />
          )}
          {withCreateButton && user.blog_count < 1 && (
            <Link className={`${styles.btn_newblog} btn btn-block`} to="/blogs/new"><i className="fa fa-plus" /> Buat Blog Baru</Link>
          )}
          {withCreateButton && user.blog_count > 1 && (
            <Link className={`${styles.btn_newblog} btn btn-block`} to="/blogs/new-premium"><i className="fa fa-plus" /> Buat Blog Baru</Link>
          )}
        </Panel.Body>
      </Panel>
    );
  }
}
