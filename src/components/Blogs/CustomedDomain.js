import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Panel,
  Button,
  Form,
  FormGroup,
  FormControl,
  ControlLabel,
  Alert,
  InputGroup
} from 'react-bootstrap';
import { loadDetail, cnameCheck, resetCnameCheck, checkAliasAvailability, resetAliasAvailability, customizeDomain } from 'redux/modules/blog';
import { isDomainName } from 'utils/validation2';
import { Loading } from './../../components';

const styles = require('./Blogs.scss');

@connect(state => ({
  blog_detail: state.blog.detail,
}), {
  cnameCheck, resetCnameCheck, checkAliasAvailability, resetAliasAvailability, customizeDomain, loadDetail
})

export default class SetupCustomDomain extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  componentDidMount() {

  }

  render() {
    const {
      blog_detail
    } = this.props;
    return (
      <div>
        a
      </div>
    );
  }
}
