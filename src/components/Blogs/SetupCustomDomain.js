import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Panel,
  Button,
  Form,
  FormGroup,
  FormControl,
  ControlLabel,
  Alert,
  InputGroup
} from 'react-bootstrap';
import { loadDetail, cnameCheck, resetCnameCheck, checkAliasAvailability, resetAliasAvailability, customizeDomain } from 'redux/modules/blog';
import { isDomainName } from 'utils/validation2';
import { Loading } from './../../components';

const styles = require('./Blogs.scss');

@connect(state => ({
  blogs: state.blog,
  user: state.auth.user,
  blog_detail: state.blog.detail,
  cname_check: state.blog.cname_check,
  alias_availability: state.blog.alias_availability,
  customize_domain: state.blog.customize_domain
}), {
  cnameCheck, resetCnameCheck, checkAliasAvailability, resetAliasAvailability, customizeDomain, loadDetail
})

export default class SetupCustomDomain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      step: 1,
      user_input: {
        domain: '',
        extension: '.id'
      }
    };
  }

  componentDidMount() {

  }

  handleFormChange(e) {
    if (e.target.value) {
      this.setState({
        user_input: {
          ...this.state.user_input,
          [e.target.name]: e.target.value
        }
      });
    } else {
      this.setState({
        user_input: {
          ...this.state.user_input,
          [e.target.name]: ''
        }
      });
    }
  }

  checkCname() {
    this.setState({
      step: this.state.step + 1
    });
  }

  async setDomain() {
    const {
      resetCnameCheck, checkAliasAvailability, resetAliasAvailability, user
    } = this.props;
    const domain = this.state.user_input.domain + this.state.user_input.extension;
    const aliasAvailability = await checkAliasAvailability(domain, user.token);
    if (this.props.alias_availability.data.is_available) {
      resetCnameCheck();
      resetAliasAvailability();
      this.setState({
        step: 2
      });
    }
  }

  prevStep() {
    this.setState({
      step: this.state.step - 1
    });
  }

  async handleCnameCheck() {
    const {
      blog_detail, user, cnameCheck
    } = this.props;
    const customDomain = this.state.user_input.domain + this.state.user_input.extension;
    const cname_check = await cnameCheck(blog_detail.data.domain, customDomain, user.token);
  }

  async handleCustomizeDomain() {
    const { blog_detail, user, customizeDomain } = this.props;
    this.setState({
      step: 3
    });
    const domain = this.state.user_input.domain + this.state.user_input.extension;
    const customize_domain = await customizeDomain(blog_detail.data.domain, domain, user.token);
    loadDetail(match.params.blog_domain, user.token);
  }

  render() {
    const {
      blog_detail, cname_check, alias_availability, customize_domain
    } = this.props;
    const isValidDomain = isDomainName(this.state.user_input.domain);
    return (
      <div>
        {this.state.step === 1 && (
          <Panel>
            <Panel.Heading className={styles.panel_header}>
              Langkah 1
              <small className={styles.subtitle_panel}>
                {' '} Tentukan Domain
              </small>
            </Panel.Heading>
            <Panel.Body>
              {(alias_availability.loaded && !alias_availability.data.is_available) && (
                <Alert bsStyle="danger">
                  <strong>Ups!</strong> Nama domain sudah digunakan di blog lain.
                </Alert>
              )}
              <p>Masukkan nama domainmu:</p>
              <Form inline onSubmit={e => { e.preventDefault(); }}>
                <FormGroup controlId="formInlineName">
                  <ControlLabel>www.</ControlLabel>{' '}
                  <FormControl
                    onChange={this.handleFormChange.bind(this)}
                    name="domain"
                    className={styles.my_field}
                    type="text"
                    placeholder="Nama Domain"
                    value={this.state.user_input.domain}
                  />
                  <FormControl
                    onChange={this.handleFormChange.bind(this)}
                    name="extension"
                    className={styles.my_field}
                    componentClass="select"
                    placeholder="select"
                    defaultValue={this.state.user_input.extension}
                  >
                    <option value=".id">.id</option>
                    <option value=".ac.id">.ac.id</option>
                    <option value=".biz.id">.biz.id</option>
                    <option value=".co.id">.co.id</option>
                    <option value=".desa.id">.desa.id</option>
                    <option value=".go.id">.go.id</option>
                    <option value=".mil.id">.mil.id</option>
                    <option value=".net.id">.net.id</option>
                    <option value=".or.id">.or.id</option>
                    <option value=".sch.id">.sch.id</option>
                    <option value=".web.id">.web.id</option>
                    <option value=".ponpes.id">.ponpes.id</option>
                  </FormControl>
                </FormGroup>{' '}
              </Form>
              {(this.state.user_input.domain && !isValidDomain) && (
                <span className={styles.invalid_domain}>Nama domain tidak sesuai!</span>
              )}
              <div className={styles.control_container}>
                <Button disabled={!this.state.user_input.domain || !isValidDomain} onClick={this.setDomain.bind(this)} type="submit">Selanjutnya <i className="fa fa-angle-right" /></Button>
              </div>
            </Panel.Body>
          </Panel>
        )}
        {this.state.step === 2 && (
          <Panel>
            <Panel.Heading className={styles.panel_header}>
                Langkah 2
              <small className={styles.subtitle_panel}>
                {' '} Konfigurasi CNAME
              </small>
            </Panel.Heading>
            <Panel.Body>
              <p>Pada situs web pendaftar domainmu, cari pengaturan DNS dan masukkan CNAME berikut:</p>
              {cname_check.loaded && (!cname_check.data.cname_status.www || !cname_check.data.cname_status.random) && (
                <span className={styles.invalid_domain}>Konfigurasi CNAME belum benar atau dalam proses propagasi.</span>
              )}
              <table className="table">
                <thead>
                  <tr>
                    <td>Host/Name</td>
                    <td>Value/Target</td>
                    <td />
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>www.{this.state.user_input.domain}{this.state.user_input.extension}</td>
                    <td>cdm.my.id</td>
                    <td>
                      {(cname_check.loaded && cname_check.data.cname_status.www) ? (
                        <i className={`fa fa-check ${styles.cname_true}`} />
                      ) : (
                        cname_check.loaded && (
                          <i className={`fa fa-times ${styles.cname_false}`} />
                        )
                      )}
                    </td>
                  </tr>
                  <tr>
                    <td>{blog_detail.data.cname_config.host}.{this.state.user_input.domain}{this.state.user_input.extension}</td>
                    <td>{blog_detail.data.cname_config.value}.cdm.my.id</td>
                    <td>
                      {(cname_check.loaded && cname_check.data.cname_status.random) ? (
                        <i className={`fa fa-check ${styles.cname_true}`} />
                      ) : (
                        cname_check.loaded && (
                          <i className={`fa fa-times ${styles.cname_false}`} />
                        )
                      )}
                    </td>
                  </tr>
                </tbody>
              </table>
              {(!cname_check.loaded || !cname_check.data.cname_status.www || !cname_check.data.cname_status.random) && (
                cname_check.loading ? (
                  <Loading />
                ) : (
                  <Button
                    className={`btn btn-block ${styles.btn_pandi}`}
                    onClick={this.handleCnameCheck.bind(this)}
                    disabled={cname_check.loading}
                  >Saya sudah melakukan konfigurasi CNAME</Button>
                )
              )}
              {(cname_check.loaded && cname_check.data.cname_status.www && cname_check.data.cname_status.random) && (
                <div className={styles.control_container}>
                  <Button onClick={this.prevStep.bind(this)} className={styles.btn_cancel}>Batal</Button>
                  <Button onClick={this.handleCustomizeDomain.bind(this)} type="submit">Selanjutnya <i className="fa fa-angle-right" /></Button>
                </div>
              )}
            </Panel.Body>
          </Panel>
        )}
        {this.state.step === 3 && (
          <Panel>
            <Panel.Body>
              {customize_domain.loading ? (
                <div>
                  <Loading />
                  <p className={styles.centering}>Mohon tunggu sebentar. Kami sedang menyiapkan domain baru blogmu.</p>
                </div>
              ) : (
                <div className={styles.centering}>
                  <h1 className={styles.custom_success}><i className="fa fa-check-circle" /></h1>
                  <p className={styles.custom_old_domain}>{blog_detail.data.domain}</p>
                  <p>Kini Menjadi</p>
                  <p className={styles.custom_new_domain}>{blog_detail.data.alias}</p>
                </div>
              )}
            </Panel.Body>
          </Panel>
        )}
      </div>
    );
  }
}
