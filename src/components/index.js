/**
 *  Point of contact for component modules
 *
 *  ie: import { CounterButton, InfoBar } from 'components';
 *
 */

export CounterButton from './CounterButton/CounterButton';
// export FacebookLogin from './FacebookLogin/FacebookLogin';
export GithubButton from './GithubButton/GithubButton';
export InfoBar from './InfoBar/InfoBar';
// export LoginForm from './LoginForm/LoginForm';
// export MiniInfoBar from './MiniInfoBar/MiniInfoBar';
export Notifs from './Notifs/Notifs';
export Provider from './Provider/Provider';
export ReduxAsyncConnect from './ReduxAsyncConnect/ReduxAsyncConnect';
// export RegisterForm from './RegisterForm/RegisterForm';
// export SurveyForm from './SurveyForm/SurveyForm';
// export WidgetForm from './WidgetForm/WidgetForm';

export MyNavbar from './MyNavbar/MyNavbar';
export NewsfeedNav from './MyNavbar/NewsfeedNav';

export MyBlogs from './Blogs/MyBlogs';
export MyBlogsSlider from './Blogs/MyBlogsSlider';
export CreateBlog from './Blogs/CreateBlog';
export CreateBlogPremium from './Blogs/CreateBlogPremium';
export SetupCustomDomain from './Blogs/SetupCustomDomain';
export CustomedDomain from './Blogs/CustomedDomain';

export ProfileCard from './Profile/ProfileCard';
export DynamicProfileCard from './Profile/DynamicProfileCard';
export ProfileEditor from './Profile/ProfileEditor';

export NewsFeed from './NewsFeed/NewsFeed';

export SubscriptionCard from './Subscription/SubscriptionCard';
export ExtendSubscription from './Subscription/ExtendSubscription';

export InvoiceTable from './Invoices/InvoiceTable';

export CartList from './Cart/CartList';
export CartSummaryInfo from './Cart/SummaryInfo';
export CartPayment from './Cart/Payment';

export DomainSearch from './Registrar/DomainSearch';
export DomainLists from './Registrar/DomainLists';
export Nameservers from './Registrar/Nameservers';
export RegistrarSideMenu from './Registrar/SideMenu';
export RegistrarHeader from './Registrar/Header';
export DomainInformationCard from './Registrar/DomainInformationCard';
export ContactForm from './Registrar/ContactForm';
export NameServerForm from './Registrar/NameServerForm';
export RecordForm from './Registrar/RecordForm';
export DomainPaymentForm from './Registrar/PaymentForm';
export RecordList from './Registrar/RecordList';
export DomainRenewal from './Registrar/DomainRenewal';
export DomainDetailCard from './Registrar/DomainDetailCard';

export Loading from './Loading/Loading';

export Footer from './Footer/Footer';
