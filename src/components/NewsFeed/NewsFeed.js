import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Panel,
  Button
} from 'react-bootstrap';
import { Loading } from './../../components';
import { Facebook } from 'react-content-loader';
import { isLoaded as isNewsfeedLoaded, load as loadNewsfeed, loadMore as loadNewsfeedMore } from 'redux/modules/newsfeed';

@connect(state => ({
  user: state.auth.user,
  newsfeeds: state.newsfeed
}), { isNewsfeedLoaded, loadNewsfeed, loadNewsfeedMore })

export default class NewsFeed extends Component {
  componentDidMount() {
    const {
      user, newsfeeds, loadNewsfeed
    } = this.props;
    if (!newsfeeds.loaded) {
      loadNewsfeed(user.token);
    }
  }

  handleLoadMore() {
    const { loadNewsfeedMore, newsfeeds } = this.props;
    loadNewsfeedMore(newsfeeds.meta.next_url);
  }

  render() {
    const { newsfeeds } = this.props;
    const styles = require('./NewsFeed.scss');
    return (
      <div>
        {newsfeeds.loaded && (
          <div>
            {newsfeeds.data.map(newsfeed => (
              <Panel key={newsfeed.post.ID + newsfeed.post.BLOG_ID}>
                <Panel.Body>
                  <div className={styles.news_user}>
                    <div className={styles.lead_left}>
                      <div className={styles.avatar}>
                        <img className={`${styles.avatar_image} img-circle`} src={newsfeed.user.profile_image_url} />
                      </div>
                    </div>
                    <div className={`${styles.lead_body} ${styles.lead_middle}`}>
                      <div>
                        <h6 className={styles.lead_heading}>
                          {newsfeed.user.dashboard_user_id ? (
                            <Link className={styles.profile_name_link} to={`/profile/${newsfeed.user.dashboard_user_id}`}>
                              {newsfeed.user.display_name}
                            </Link>
                          ) : (
                            <span>{newsfeed.user.display_name}</span>
                          )}
                        </h6>
                        <small className="text-muted">{newsfeed.post.post_date}</small>
                      </div>
                    </div>
                  </div>
                </Panel.Body>
                <Panel.Body className={styles.content}>
                  <div className="img_container">
                    {newsfeed.post.thumbnail && (
                      <a target="_blank" className="btn-block" href={newsfeed.post.guid}>
                        <img src={newsfeed.post.thumbnail} title={newsfeed.post.post_title} width="100%" />
                      </a>
                    )}
                    <a className="btn-block" href={newsfeed.post.guid}>
                      <h4>{newsfeed.post.post_title}</h4>
                    </a>
                    <p>{newsfeed.post.resume}</p>
                    <small className="text-muted">{newsfeed.blog.domain}</small>
                  </div>
                </Panel.Body>
              </Panel>
            ))}
            {!newsfeeds.loading && newsfeeds.meta.next_url && (
              <Button onClick={this.handleLoadMore.bind(this)} className={`${styles.btn_loadmore} btn-block btn`}>Muat Selengkapnya</Button>
            )}

            {!newsfeeds.loading && !newsfeeds.meta.next_url && (
              <div className={styles.centering}>
                <i className={`fa fa-list ${styles.icon_big_disabled}`} />
                <p>Tidak ada artikel.</p>
              </div>
            )}
          </div>
        )}
        {newsfeeds.loading && (
          <Facebook />
        )}
      </div>
    );
  }
}
