import React, { Component } from 'react';
import ReactLoading from 'react-loading';

export default class Loading extends Component {
  render() {
    const styles = require('./Loading.scss');
    return (
      <ReactLoading type="bars" color="red" delay={0} className={styles.loading_container} />
    );
  }
}
