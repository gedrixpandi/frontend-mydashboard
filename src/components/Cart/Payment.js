import React, { Component } from 'react';
import { Panel, Button } from 'react-bootstrap';
import { load as loadOrder, checkout as checkoutOrder } from 'redux/modules/order';
import { load as loadInvoice, count as countInvoice } from 'redux/modules/invoice';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';

const logo_bca = require('./../../theme/img/bca.png');
const styles = require('./Cart.scss');

const DEFAULT_BANK_CODE = '014';

@connect(
  state => ({
    user: state.auth.user,
    checkout: state.order.checkout
  }),
  {
    checkoutOrder, loadOrder, loadInvoice, countInvoice, pushState: push
  }
)
export default class Payment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bank_code: DEFAULT_BANK_CODE
    };

    this.changePayment = this.changePayment.bind(this);
    this.pay = this.pay.bind(this);
  }

  changePayment(e) {
    this.setState({
      bank_code: e.target.value
    });
  }

  async pay() {
    const {
      loadOrder, checkoutOrder, user, pushState, loadInvoice, countInvoice
    } = this.props;
    const data = {
      bank_code: this.state.bank_code
    };
    await checkoutOrder(data, user.token);
    console.log(this.props.checkout);
    if (this.props.checkout.data.id) {
      loadInvoice(user.token);
      countInvoice(user.token);
      pushState(`invoices/${this.props.checkout.data.id}`);
      loadOrder(user.token);
    }
  }

  render() {
    const { checkout } = this.props;
    return (
      <Panel className={styles.panel}>
        <Panel.Heading className={styles.header}>Pembayaran</Panel.Heading>
        <Panel.Body>
          <div>
            <input type="radio" name="bank_code" value="014" checked />
            {'  '}
            <img alt="BCA" src={logo_bca} width="50" />
          </div>
          <hr />
          <Button
            onClick={this.pay}
            className="btn btn-block btn-warning btn-lg"
            disabled={checkout.loading}
          >
            Bayar
          </Button>
        </Panel.Body>
      </Panel>
    );
  }
}
