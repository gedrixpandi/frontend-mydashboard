import React, { Component } from 'react';
import { Panel } from 'react-bootstrap';

const styles = require('./Cart.scss');

export default class SummaryInfo extends Component {
  formatCurrency(number) {
    return new Intl.NumberFormat('id-ID', {
      style: 'currency',
      currency: 'IDR'
    }).format(number);
  }

  render() {
    const { activeOrder } = this.props;
    const subTotal = activeOrder.total;
    const tax = activeOrder.total * (10 / 100);
    const total = subTotal + tax;
    return (
      <Panel className={styles.panel}>
        <Panel.Heading className={styles.header}>Ringkasan Informasi</Panel.Heading>
        <Panel.Body>
          <table className="table table-borderless">
            <tbody>
              <tr>
                <td>
                  <b>Subtotal</b>
                </td>
                <td>
                  {this.formatCurrency(subTotal)}
                </td>
              </tr>
              <tr>
                <td>
                  <b>PPN 10%</b>
                </td>
                <td>
                  {this.formatCurrency(tax)}
                </td>
              </tr>
              <tr>
                <td>
                  <b>Total</b>
                </td>
                <td>
                  {this.formatCurrency(total)}
                </td>
              </tr>
            </tbody>
          </table>
        </Panel.Body>
      </Panel>
    );
  }
}
