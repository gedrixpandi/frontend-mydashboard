import React, { Component } from 'react';
import { Panel } from 'react-bootstrap';
import { deleteItem, load as loadOrder } from 'redux/modules/order';
import { connect } from 'react-redux';

const styles = require('./Cart.scss');

@connect(
  state => ({
    user: state.auth.user,
  }),
  {
    deleteItem, loadOrder
  }
)
export default class CartList extends Component {
  async handleDeleteItem(orderDetailId) {
    const { user, deleteItem, loadOrder } = this.props;
    const deleteData = {
      order_detail_id: orderDetailId
    };
    await deleteItem(deleteData, user.token);
    loadOrder(user.token);
  }

  render() {
    const { orderDetails } = this.props;
    return (
      <Panel className={styles.panel}>
        <Panel.Heading className={styles.header}>Daftar Pesanan</Panel.Heading>
        <Panel.Body>
          <table className="table table-borderless table-striped">
            <tbody>
              {orderDetails &&
                orderDetails.map(item => (
                  <tr key={item.id}>
                    <td>
                      <b>{item.product_name}</b>
                      {item.bundle && (
                        <ul className={styles.bundle_list}>
                          {item.bundle.map(bundle => (
                            <li className={styles.bundle_list_name}>
                              + {bundle.product_name}&nbsp;&nbsp;&nbsp;
                              x{bundle.qty}&nbsp;&nbsp;&nbsp;
                              Rp{bundle.price.fmt}
                            </li>
                          ))}
                        </ul>
                      )}
                    </td>
                    <td>x{item.qty}</td>
                    <td>
                      Rp
                      {item.price.fmt}
                    </td>
                    <td>
                      <button
                        className={styles.button_trash}
                        onClick={() => { this.handleDeleteItem(item.id); }}
                      >
                        <i className="fa fa-trash" /></button>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        </Panel.Body>
      </Panel>
    );
  }
}
