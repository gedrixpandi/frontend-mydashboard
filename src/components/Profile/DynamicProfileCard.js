import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { follow, unfollow, switchFollowedStatus } from 'redux/modules/profile';
import {
  Panel,
  ListGroup,
  ListGroupItem,
  Label,
  ButtonGroup,
  Button,
  Image
} from 'react-bootstrap';

@connect(state => ({
  user: state.auth.user,
  profile: state.profile.profile
}), { follow, unfollow, switchFollowedStatus })

export default class ProfileCard extends Component {
  followHandler() {
    const {
      user, profile, follow, switchFollowedStatus
    } = this.props;
    follow(user.token, profile.data.hash_id);
    switchFollowedStatus();
  }

  unfollowHandler() {
    const {
      user, profile, unfollow, switchFollowedStatus
    } = this.props;
    unfollow(user.token, profile.data.hash_id);
    switchFollowedStatus();
  }

  render() {
    const { profile, user } = this.props;
    const styles = require('./ProfileCard.scss');
    return (
      <Panel className={styles.panel}>
        <Panel.Body>
          <div className={styles.avatar_container}>
            <Image src={profile.data.photo} width="50%" circle />
          </div>
          <div className={styles.profile_name_container}>
            {profile.data.full_name}
          </div>
          <div className={styles.profile_body}>
            {(profile.data.id != user.id) ? (
              profile.data.is_followed ? (
                <Button
                  className={`${styles.big_unfollow_button} btn btn-block`}
                  onClick={this.unfollowHandler.bind(this)}
                >
                  <i className="fa fa-user-plus" /> Batal Ikuti
                </Button>
              ) : (
                <Button
                  className={`${styles.big_follow_button} btn btn-block`}
                  onClick={this.followHandler.bind(this)}
                >
                  <i className="fa fa-user-plus" /> Ikuti
                </Button>
              )
            ) : (
              <a className={`${styles.profile_edit_button} btn btn-block`} href="https://u.id/pengguna/profil"><i className="fa fa-edit" /> Ubah Profil</a>
            )}
          </div>
        </Panel.Body>
        <Panel.Body>
          <div className="row">
            <div className={`${styles.border_right} col-lg-4 col-md-4 col-sm-4 col-xs-4`}>
              <div className={styles.counter}>
                <div className={styles.count}>{profile.data.blog_count}</div>
                <div className="text-muted">Blog</div>
              </div>
            </div>
            <div className={`${styles.border_right} col-lg-4 col-md-4 col-sm-4 col-xs-4`}>
              <div className={styles.counter}>
                <div className={styles.count}>{profile.data.followers || 0}</div>
                <div className="text-muted">Pengikut</div>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4">
              <div className={styles.counter}>
                <div className={styles.count}>{profile.data.followings || 0}</div>
                <div className="text-muted">Mengikuti</div>
              </div>
            </div>
          </div>
        </Panel.Body>
      </Panel>
    );
  }
}
