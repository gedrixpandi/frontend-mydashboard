import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Panel,
  ListGroup,
  ListGroupItem,
  Label,
  ButtonGroup,
  Button,
  Image,
  FormControl
} from 'react-bootstrap';
import { Editor, EditorState } from 'draft-js';

@connect(state => ({
  user: state.auth.user
}), { })

export default class ProfileEditor extends Component {
  constructor(props) {
    super(props);
    const { user } = props;
    this.state = {
      profile_values: {
        full_name: user.full_name,
        about: EditorState.createEmpty()
      }
    };
  }

  onAboutChange(editorState) {
    this.setState({
      profile_values: {
        ...this.state.profile_values,
        about: editorState
      }
    });
  }

  onChange(e) {
    this.setState({
      profile_values: {
        ...this.state.profile_values,
        full_name: e.target.value
      }
    });
  }

  render() {
    const { user } = this.props;
    const styles = require('./ProfileCard.scss');
    return (
      <Panel className={styles.panel}>
        <Panel.Body>
          <div className={styles.avatar_container}>
            <Image src={user.photo} width="50%" circle />
          </div>
          <div className={styles.profile_name_container}>
            <FormControl
              onChange={this.onChange.bind(this)}
              name="full_name"
              className={styles.my_field}
              type="text"
              placeholder="Nama Lengkap"
              value={this.state.profile_values.full_name}
            />
            <Editor
              textAlignment="left"
              editorState={this.state.profile_values.about}
              onChange={this.onAboutChange.bind(this)}
            />
          </div>
        </Panel.Body>
        <Panel.Body>
          <div className="row">
            <div className={`${styles.border_right} col-lg-4 col-md-4 col-sm-4 col-xs-4`}>
              <div className={styles.counter}>
                <div className={styles.count}>{user.blog_count}</div>
                <div className="text-muted">Blog</div>
              </div>
            </div>
            <div className={`${styles.border_right} col-lg-4 col-md-4 col-sm-4 col-xs-4`}>
              <div className={styles.counter}>
                <div className={styles.count}>{user.followers || 0}</div>
                <div className="text-muted">Pengikut</div>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4">
              <div className={styles.counter}>
                <div className={styles.count}>{user.followings || 0}</div>
                <div className="text-muted">Mengikuti</div>
              </div>
            </div>
          </div>
        </Panel.Body>
      </Panel>
    );
  }
}
