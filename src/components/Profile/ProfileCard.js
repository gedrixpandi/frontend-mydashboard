import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Panel,
  ListGroup,
  ListGroupItem,
  Label,
  ButtonGroup,
  Button,
  Image
} from 'react-bootstrap';

@connect(state => ({
  user: state.auth.user
}))

export default class ProfileCard extends Component {
  render() {
    const { user } = this.props;
    const styles = require('./ProfileCard.scss');
    return (
      <Panel className={styles.panel}>
        <Panel.Body>
          <div className={styles.avatar_container}>
            <Image src={user.photo} width="50%" circle />
          </div>
          <div className={styles.profile_name_container}>
            {user.full_name}
          </div>
          {1 === 0 && (
            <div className={styles.profile_body}>
              <Link className={`${styles.profile_edit_button} btn btn-block`} to="/profile/edit"><i className="fa fa-edit" /> Ubah Profil</Link>
            </div>
          )}
        </Panel.Body>
        <Panel.Body>
          <div className="row">
            <div className={`${styles.border_right} col-lg-4 col-md-4 col-sm-4 col-xs-4`}>
              <div className={styles.counter}>
                <div className={styles.count}>{user.blog_count}</div>
                <div className="text-muted">Blog</div>
              </div>
            </div>
            <div className={`${styles.border_right} col-lg-4 col-md-4 col-sm-4 col-xs-4`}>
              <div className={styles.counter}>
                <div className={styles.count}>{user.followers || 0}</div>
                <div className="text-muted">Pengikut</div>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-4">
              <div className={styles.counter}>
                <div className={styles.count}>{user.followings || 0}</div>
                <div className="text-muted">Mengikuti</div>
              </div>
            </div>
          </div>
        </Panel.Body>
      </Panel>
    );
  }
}
