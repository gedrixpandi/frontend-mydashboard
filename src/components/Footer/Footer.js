import React, { Component } from 'react';
import config from './../../config';

const styles = require('./Footer.scss');

export default class Footer extends Component {
  render() {
    return (
      <div className={styles.footer}>
        <div className={styles.footer_menu}>
          <a href="https://www.my.id/tentang-my-id/">Tentang My.ID</a>
          <a href="https://www.my.id/syarat-ketentuan/">Syarat &amp; Ketentuan</a>
          <a className="last" href="https://www.my.id/faq/">FAQ</a>
        </div>
        <div className={styles.footer_copyright}>&copy;2018 My.ID - My Digital Life</div>
        <div className={styles.footer_copyright}>Didukung oleh PANDI</div>
        <div className={styles.footer_copyright}>Version: {config.appVersion}</div>
      </div>
    );
  }
}
