import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Panel,
  ListGroup,
  ListGroupItem,
  Label,
  ButtonGroup,
  Button,
  ButtonToolbar,
  ToggleButtonGroup,
  ToggleButton
} from 'react-bootstrap';
import ReactLoading from 'react-loading';
import { push } from 'react-router-redux';
import { loadPackage, renew as renewSubscription } from 'redux/modules/subscription';
import { load as loadInvoice, count as countInvoice } from 'redux/modules/invoice';

@connect(state => ({
  user: state.auth.user,
  subscription: state.subscription,
  subscription_package: state.subscription.package,
  subscription_renew: state.subscription.renew
}), {
  loadPackage, renewSubscription, loadInvoice, countInvoice, pushState: push
})

export default class ExtendSubscription extends Component {
  constructor(props) {
    super(props);

    this.state = {
      period: 1,
      discount: 0,
      total: 0,
      payment_method: '014',
    };
  }

  componentDidMount() {
    this.packageInitiate();
  }

  async packageInitiate() {
    const { user, loadPackage } = this.props;
    const package_data = await loadPackage(user.token);
    this.setState({
      total: this.props.subscription_package.data.price.number * this.state.period
    });
  }

  handlePeriodChange(e) {
    const { subscription_package } = this.props;

    const total = (subscription_package.data.price.number * e) - this.state.discount;
    this.setState({ period: e, total });
  }

  changePaymentMethod(id) {
    this.setState({
      payment_method: id
    });
  }

  async handleRenew() {
    const {
      subscription_package,
      renewSubscription,
      user,
      pushState,
      countInvoice,
      loadInvoice
    } = this.props;

    const data = {
      qty: this.state.period,
      bank_code: this.state.payment_method,
      product_id: subscription_package.data.id
    };

    const renew_status = await renewSubscription(data, user.token);
    countInvoice(user.token);
    loadInvoice(user.token);
    pushState(`/invoices/${this.props.subscription_renew.data.invoice.id}`);
  }

  render() {
    const { subscription_package, subscription_renew } = this.props;
    const styles = require('./SubscriptionCard.scss');
    const logo_bca = require('./img/bca.png');
    const logo_mandiri = require('./img/mandiri.png');
    const currencyFormatter = require('currency-formatter');
    return (
      <Panel className={styles.panel}>
        <Panel.Heading className={styles.header}>Perpanjang Langganan</Panel.Heading>
        <Panel.Body>
          {subscription_package.loaded && (
            <Panel>
              <Panel.Body>
                Harga per tahun Rp {currencyFormatter.format(subscription_package.data.price.number, { code: 'Rp ' })}
              </Panel.Body>
            </Panel>
          )}
          <h3>Pilih Periode Langganan</h3>
          <ButtonToolbar>
            <ToggleButtonGroup
              type="radio"
              name="period"
              onChange={this.handlePeriodChange.bind(this)}
              value={this.state.period}
            >
              <ToggleButton value={1}>1 Tahun</ToggleButton>
              <ToggleButton value={2}>2 Tahun</ToggleButton>
              <ToggleButton value={3}>3 Tahun</ToggleButton>
            </ToggleButtonGroup>
          </ButtonToolbar>
          <br />
          <table className="table">
            <tbody>
              <tr>
                <td>Lama Langganan</td>
                <td>{this.state.period} Tahun</td>
              </tr>
              <tr>
                <td><strong>Total Tagihan</strong></td>
                <td><strong>Rp { currencyFormatter.format(this.state.total, { code: 'Rp ' }) }</strong></td>
              </tr>
            </tbody>
          </table>
          <Panel>
            <Panel.Heading>Metode Pembayaran</Panel.Heading>
            <Panel.Body>
              <ButtonToolbar>
                <ToggleButtonGroup type="radio" name="options" defaultValue="014">
                  <ToggleButton value="014" onClick={this.changePaymentMethod.bind(this, 1)}><img src={logo_bca} width="50" /> Transfer Bank BCA</ToggleButton>
                </ToggleButtonGroup>
              </ButtonToolbar>
            </Panel.Body>
          </Panel>
          {subscription_renew.loading ? (
            <ReactLoading type="bars" color="red" delay={0} className={styles.loading_container} />
          ) : (
            <Button onClick={this.handleRenew.bind(this)} className="btn btn-block btn-warning btn-lg">Bayar</Button>
          )}
        </Panel.Body>
      </Panel>
    );
  }
}
