import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Panel,
  ListGroup,
  ListGroupItem,
  Label,
  ButtonGroup,
  Button
} from 'react-bootstrap';
import ReactLoading from 'react-loading';
import Moment from 'react-moment';
import 'moment/locale/id';
import { isLoaded as isSubscriptionLoaded, load as loadSubscription } from 'redux/modules/subscription';

@connect(state => ({
  subscription: state.subscription,
  user: state.auth.user,
}), { isSubscriptionLoaded, loadSubscription })

export default class SubscriptionCard extends Component {
  componentDidMount() {
    const { loadSubscription, subscription, user } = this.props;
    loadSubscription(user.token);
  }

  render() {
    const { online, withExtendButton, subscription } = this.props;
    const styles = require('./SubscriptionCard.scss');
    return (
      <Panel className={styles.panel}>
        <Panel.Heading className={styles.header}>Status Langganan</Panel.Heading>
        <Panel.Body>
          {subscription.loaded ? (
            <div>
              <h3 className={subscription.data.status === 'Premium' ? styles.status_premium : styles.status_free}>{subscription.data.status}</h3>
              {subscription.data.status === 'Premium' && (
                <p>Aktif Sampai: <Label><Moment
                  locale="id"
                  format="D MMMM YYYY"
                  subtract={{ days: 1 }}
                >
                  {subscription.data.expired_at}
                </Moment></Label></p>
              )}
              {withExtendButton && (
                <div>
                  {subscription.data.status === 'Free' ? (
                    <div>
                      <p className="text-muted">Berlangganan premium untuk mendapatkan fitur lebih.</p>
                      {(1 === 2) && (<Link className={`${styles.btn_extend} btn btn-block`} to="/subscription">Langganan</Link>)}
                    </div>
                  ) : (
                    <Link className={`${styles.btn_extend} btn btn-block`} to="/subscription">Perpanjang</Link>
                  )}
                </div>
              )}
            </div>
          ) : (
            <ReactLoading type="bars" color="red" delay={0} className={styles.loading_container} />
          )}
        </Panel.Body>
      </Panel>
    );
  }
}
