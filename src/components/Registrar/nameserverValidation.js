const nameserverValidation = values => {
  const errors = {};
  if (!values.nameservers || !values.nameservers.length || values.nameservers.length < 2) {
    errors.nameservers = { _error: 'Minimal harus memiliki 2 Name Server' };
  } else {
    const nameserverArrayErrors = [];
    values.nameservers.forEach((nameserver, nameserverIndex) => {
      let nameserverErrors = null;
      if (!nameserver) {
        nameserverErrors = 'Wajib Diisi';
        nameserverArrayErrors[nameserverIndex] = nameserverErrors;
      } else if (nameserver && !/^(?!:\/\/)([a-zA-Z0-9-_]+\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\.[a-zA-Z]{2,11}?$/i.test(nameserver)) {
        nameserverErrors = 'Format name server salah';
        nameserverArrayErrors[nameserverIndex] = nameserverErrors;
      }
    });
    if (nameserverArrayErrors.length) {
      errors.nameservers = nameserverArrayErrors;
    }
  }
  return errors;
};

export default nameserverValidation;
