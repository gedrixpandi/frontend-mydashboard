import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Panel,
  ListGroup,
  ListGroupItem,
  Label,
  ButtonGroup,
  Button
} from 'react-bootstrap';
import ReactLoading from 'react-loading';

const styles = require('./Registrar.scss');

@connect(state => ({
  user: state.auth.user
}))

export default class SideMenu extends Component {
  render() {
    return (
      <div className={styles.side_menu_container}>
        <Link to="/registrar/domain/search" className={`btn ${styles.btn_search_domain}`}>Cari Domain</Link>
      </div>
    );
  }
}
