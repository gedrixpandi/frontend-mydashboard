import { createValidator, required, maxLength, integer, oneOf } from 'utils/validation';

export const colors = ['Blue', 'Fuchsia', 'Green', 'Orange', 'Red', 'Taupe'];

const DomainSearchValidation = createValidator({
  domain_name: [required],
  extension: [required]
});
export default DomainSearchValidation;
