import React, { Component } from 'react';
import { FieldArray, Field, reduxForm } from 'redux-form';
import { Button } from 'react-bootstrap';
import nameserverValidation from './nameserverValidation';

const styles = require('./Registrar.scss');

const MAX_NAMESERVER = 7;

const renderField = ({
  input, label, type, fields, index, withRemoveButton, styles, meta: { touched, error }
}) => (
  <div className="form-inline">
    <div className={`form-group ${error && touched ? 'has-error' : ''}`}>
      <input {...input} type={type} className="form-control" placeholder={label} />
      {withRemoveButton && (
        <button
          type="button"
          className={`btn btn-xs btn-danger ${styles.ml_5}`}
          onClick={() => fields.remove(index)}
        ><i className="fa fa-times" /></button>
      )}
      {error && touched && (
        <span className={`${styles.ml_5} ${styles.color_red}`}>{error}</span>
      )}
    </div>
  </div>
);

const renderNameServers = ({ fields, styles, meta: { error } }) => (
  <div>
    {error && <span className="help-block">{error}</span>}
    {fields.map((ns, index) => (
      <div key={index} className={styles.mb_5}>
        <Field
          name={ns}
          type="text"
          component={renderField}
          label={`Name server ${index + 1}`}
          fields={fields}
          index={index}
          withRemoveButton={index > 1}
          styles={styles}
        />
      </div>
    ))}
    {fields.length < MAX_NAMESERVER && (
      <button
        type="button"
        className={`${styles.btn_new_ns} ${styles.mb_5}`}
        onClick={() => fields.push()}
      >
          Tambah Name server...
      </button>
    )}
  </div>
);

@reduxForm({
  form: 'nameservers',
  validate: nameserverValidation,
})

export default class NameServerForm extends Component {
  render() {
    const { handleSubmit } = this.props;
    return (
      <div className={styles.contact_form_container}>
        <form onSubmit={handleSubmit}>
          <FieldArray styles={styles} name="nameservers" component={renderNameServers} />
          <div className={styles.fl_right}>
            {this.props.cancel && (
              <Button onClick={this.props.cancel} className={styles.btn_cancel}>{this.props.cancelText || 'Batal'}</Button>
            )}
            <Button disabled={this.props.disableSubmitButton} type="submit" className={`btn ${styles.btn_pandi} ${styles.fl_right}`}>{this.props.submitButton || 'Simpan'}</Button>
          </div>
        </form>
      </div>
    );
  }
}
