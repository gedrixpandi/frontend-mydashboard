import { createValidator, required, maxLength, integer, oneOf } from 'utils/validation';

const RecordValidation = createValidator({
  host: [],
  ipv4: [required]
});
export default RecordValidation;
