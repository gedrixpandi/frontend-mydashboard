import React, { Component } from 'react';
import { connect } from 'react-redux';
import { load as loadRecord, deleteRecord } from 'redux/modules/registrar/record';
import { Loading } from './../../components';

const styles = require('./Registrar.scss');

@connect(state => ({
  user: state.auth.user,
  record_list: state.record.list,
  delete_record: state.record.delete
}), { loadRecord, deleteRecord })

export default class RecordList extends Component {
  constructor(props) {
    super(props);
    this.deleteRecordButton = [];
    this.deleteRecord = this.deleteRecord.bind(this);
  }

  componentWillMount() {
    const { user: { token }, loadRecord, domainName } = this.props;

    loadRecord(domainName, token);
  }

  async deleteRecord(domainName, recordId) {
    const { user: { token }, loadRecord, deleteRecord } = this.props;
    this.deleteRecordButton[recordId].setAttribute('disabled', true);
    this.deleteRecordButton[recordId].innerHTML = 'Menghapus...';

    await deleteRecord(domainName, recordId, token);
    loadRecord(domainName, token);
  }

  render() {
    const { domainName } = this.props;
    return (
      <div>
        <table className="table table-borderless">
          <thead>
            <tr className={styles.table_header}>
              <td>Host</td>
              <td className={styles.centering}>Type</td>
              <td className={styles.centering}>TTL</td>
              <td className={styles.centering}>Priority</td>
              <td className={styles.centering}>Value</td>
              <td />
            </tr>
          </thead>
          {this.props.record_list.loaded && (
            <tbody>
              {this.props.record_list.data.map(record => (
                <tr key={record.id} className={`${styles.body_data}`}>
                  <td>{record.name}</td>
                  <td className={styles.centering}>{record.type}</td>
                  <td className={styles.centering}>{record.ttl}</td>
                  <td className={styles.centering}>{record.prio}</td>
                  <td className={styles.centering}>{record.content}</td>
                  <td>
                    <button
                      className={`btn btn-sm ${styles.btn_pandi_sm}`}
                      ref={ref => { this.deleteRecordButton[record.id] = ref; }}
                      onClick={() => {
                        this.deleteRecord(domainName, record.id);
                      }}
                    >Hapus</button>
                  </td>
                </tr>
              ))}
            </tbody>
          )}
        </table>
        {this.props.record_list.loading && (
          <Loading />
        )}
      </div>
    );
  }
}
