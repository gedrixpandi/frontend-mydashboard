import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import ReactLoading from 'react-loading';

const styles = require('./Registrar.scss');

@connect(state => ({
  user: state.auth.user
}))

export default class Header extends Component {
  render() {
    return (
      <div className={`col-lg-6 ${styles.page_title}`}>
        <div className="col-lg-3">
          MyDomain<span className={styles.beta_tag}>BETA</span>
        </div>
        {this.props.desc && (
          <div className="col-lg-9">
            <span className={styles.page_title_desc}>{this.props.desc}</span>
          </div>
        )}
      </div>
    );
  }
}
