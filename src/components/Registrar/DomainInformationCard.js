import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Panel } from 'react-bootstrap';
import { loadUidProfile } from 'redux/modules/profile';
import { ContactForm, NameServerForm, DomainPaymentForm } from './../../components';
import { appendDomainBuyFormData, bookDomain } from 'redux/modules/registrar/domain';
import { load as loadOrder, addItem as addOrderItem } from 'redux/modules/order';
import { withCookies } from 'react-cookie';

const styles = require('./Registrar.scss');

const STEP_CONTACT = 1,
  STEP_NAMESERVERS = 2,
  STEP_PAYMENT = 3;

const DEFAULT_NAMESERVERS = [
  'ns1.domain.id',
  'ns2.domain.id'
];

@connect(state => ({
  user: state.auth.user,
  domain_search_form: state.domain.domain_search_form,
  domain_search: state.domain.domain_search,
  uid_profile: state.profile.uid_profile.data,
  domain_buy_form: state.domain.domain_buy_form,
  domain_book: state.domain.domain_book,
  item_added: state.order.item_added
}), {
  pushState: push, loadUidProfile, appendDomainBuyFormData, bookDomain, loadOrder, addOrderItem
})
@withCookies

export default class DomainInformationCard extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCancel = this.handleCancel.bind(this);
    this.handlePayment = this.handlePayment.bind(this);
    this.addToCart = this.addToCart.bind(this);

    this.state = {
      initial_values: {},
      step: STEP_CONTACT
    };
  }

  componentDidMount() {
    this.handleRedirect();
  }

  handleRedirect() {
    const {
      pushState,
      cookies
    } = this.props;

    if (!this.props.domain_buy_form.domain_name && !cookies.get('pending_book_domain')) {
      pushState('/registrar/domain/search');
    }
  }

  handleSubmit(data) {
    const {
      appendDomainBuyFormData, domain_buy_form
    } = this.props;

    if (this.state.step === STEP_CONTACT) {
      appendDomainBuyFormData(data);
      this.setState({ step: STEP_NAMESERVERS });
    } else if (this.state.step === STEP_NAMESERVERS) {
      this.setState({ step: STEP_PAYMENT });
      appendDomainBuyFormData(data);
    } else if (this.state.step === STEP_PAYMENT) {
      // TODO: Process all
      console.log(domain_buy_form);
    }
  }

  handleCancel() {
    if (this.state.step === STEP_PAYMENT) {
      this.setState({ step: STEP_NAMESERVERS });
    } else if (this.state.step === STEP_NAMESERVERS) {
      this.setState({ step: STEP_CONTACT });
    }
  }

  async handlePayment() {
    const {
      domain_buy_form, bookDomain, user, pushState
    } = this.props;

    const book = await bookDomain(domain_buy_form, user.token);
    pushState(`/invoices/${this.props.domain_book.data.id}`);
  }
  async addToCart() {
    const {
      domain_buy_form, user, pushState, addOrderItem
    } = this.props;

    domain_buy_form.order_type = 'domain_new';

    await addOrderItem(domain_buy_form, user.token);
    pushState('/cart');
  }

  render() {
    const { item_added, domain_buy_form } = this.props;
    return (
      <div className={`${styles.domain_information_card_container} col-lg-8 col-lg-offset-2`}>
        <Panel>
          <Panel.Heading>
            <Panel.Title componentClass="h2">
              {domain_buy_form.domain_name}
            </Panel.Title>
          </Panel.Heading>
          <Panel.Body>
            {this.state.step === STEP_CONTACT && (
              <div>
                <Panel.Title componentClass="h3"><i className="fa fa-user-circle" /> Informasi Kontak</Panel.Title>
                {domain_buy_form.domain_name && (
                  <ContactForm initialValues={domain_buy_form} onSubmit={this.handleSubmit} submitButton="Selanjutnya" />
                )}
              </div>
            )}
            {this.state.step === STEP_NAMESERVERS && (
              <div>
                <Panel.Title componentClass="h3"><i className="fa fa-server" /> Name Servers</Panel.Title>
                <NameServerForm
                  initialValues={{ nameservers: domain_buy_form.nameservers }}
                  onSubmit={this.addToCart}
                  submitButton="Masukkan Keranjang"
                  cancel={this.handleCancel}
                  cancelText="Sebelumnya"
                  disableSubmitButton={item_added.loading}
                />
              </div>
            )}
            {this.state.step === STEP_PAYMENT && (
              <div>
                <Panel.Title componentClass="h3"><i className="fa fa-credit-card" /> Pembayaran</Panel.Title>
                <DomainPaymentForm onSubmit={this.handlePayment} submitButton="Bayar" loading={this.props.domain_book.loading} />
              </div>
            )}
          </Panel.Body>
        </Panel>
      </div>
    );
  }
}
