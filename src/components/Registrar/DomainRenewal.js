import React, { Component } from 'react';
import { Panel, FormGroup, Button, FormControl, Alert } from 'react-bootstrap';
import { addItem } from 'redux/modules/order';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { Loading } from './../../components';

const styles = require('./Registrar.scss');

@connect(
  state => ({
    user: state.auth.user,
    item_added: state.order.item_added
  }),
  {
    addItem, pushState: push
  }
)
export default class DomainRenewal extends Component {
  constructor(props) {
    super(props);
    this.handleRenew = this.handleRenew.bind(this);
    this.state = {
      period: 1
    };
    this.handleChangePeriod = this.handleChangePeriod.bind(this);
  }

  async handleRenew() {
    const {
      domainDetail, addItem, user, pushState
    } = this.props;
    const RenewalData = {
      domain_id: domainDetail.id,
      order_type: 'domain_renew',
      year: this.state.period
    };

    await addItem(RenewalData, user.token);
    if (this.props.item_added.loaded) {
      pushState('/cart');
    }
  }

  handleChangePeriod(e) {
    this.setState({
      period: e.target.value
    });
  }

  render() {
    const { domainDetail, item_added } = this.props;
    const { product } = domainDetail;
    const periodItems = [];

    for (let i = 1; i <= 10; i++) {
      periodItems.push(<option value={i}>{i} Tahun</option>);
    }

    return (
      this.props.domainDetail.id ? (
        <Panel className={styles.panel}>
          <Panel.Heading className={styles.panel_header}>Renew Domain</Panel.Heading>
          <Panel.Body>
            {item_added.error && (
              <Alert bsStyle="danger">
                <strong>Ups!</strong>
                <ul>
                  {item_added.error.messages.map(message => (
                    <li>{message}</li>
                  ))}
                </ul>
              </Alert>
            )}
            <p>Harga: Rp {product.price.renew.fmt}/tahun</p>
            <FormGroup>
              <FormControl
                componentClass="select"
                placeholder="select"
                onChange={this.handleChangePeriod}
              >
                {periodItems}
              </FormControl>
            </FormGroup>
            <Button
              onClick={this.handleRenew}
              className="btn btn-block btn-warning btn-lg"
            >
            Renew
            </Button>
          </Panel.Body>
        </Panel>
      ) : (
        <Loading />
      )
    );
  }
}
