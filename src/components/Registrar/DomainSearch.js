import React, { Component } from 'react';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import {
  Form,
  FormGroup,
  FormControl,
  Button,
  Table,
  InputGroup,
  DropdownButton,
  MenuItem
} from 'react-bootstrap';
import {
  loadProduct,
  isProductLoaded,
  updateDomainSearchForm,
  searchDomain,
  flushSearchData,
  appendDomainBuyFormData,
  changeDomainBooked
} from 'redux/modules/registrar/domain';
import { loadUidProfile } from 'redux/modules/profile';
import { Loading } from './../../components';
import { isDomainName2 } from 'utils/validation2';
import { withCookies } from 'react-cookie';
import { BulletList } from 'react-content-loader';

const styles = require('./Registrar.scss');

@connect(state => ({
  user: state.auth.user,
  domain_products: state.domain.products,
  domain_search_form: state.domain.domain_search_form,
  domain_search: state.domain.domain_search,
  uid_profile: state.profile.uid_profile.data
}), {
  loadProduct,
  isProductLoaded,
  updateDomainSearchForm,
  searchDomain,
  flushSearchData,
  appendDomainBuyFormData,
  loadUidProfile,
  changeDomainBooked,
  pushState: push
})
@withCookies

export default class DomainSearch extends Component {
  constructor(props, context) {
    super(props, context);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleBuy = this.handleBuy.bind(this);
    this.handleExtensionChange = this.handleExtensionChange.bind(this);

    this.state = {
      is_invalid_domain: {
        status: false,
        message: ''
      },
      buy_process: false
    };
  }

  componentWillMount() {
    const { loadProduct, flushSearchData } = this.props;
    flushSearchData();
    loadProduct();
  }

  handleChange(e) {
    const { updateDomainSearchForm } = this.props;
    this.setState({ is_invalid_domain: { status: false, message: '' } });
    updateDomainSearchForm(e);
  }

  handleExtensionChange(extension) {
    const { updateDomainSearchForm } = this.props;
    const e = {};
    e.target = {
      name: 'extension',
      value: extension
    };
    updateDomainSearchForm(e);
  }

  handleSubmit(e) {
    const { domain_search_form, searchDomain } = this.props;
    const domainName = domain_search_form.domain_name + domain_search_form.extension;
    const isValidDomain = isDomainName2(domain_search_form.domain_name);
    e.preventDefault();

    if (domain_search_form.domain_name && isValidDomain) {
      this.setState({ is_invalid_domain: { status: false, message: '' } });
      searchDomain(domainName);
    } else if (domain_search_form.domain_name.length < 5) {
      this.setState({
        is_invalid_domain: {
          status: true,
          message: 'Nama Domain Minimal 5 Karakter!'
        }
      });
    } else {
      this.setState({
        is_invalid_domain: {
          status: true,
          message: 'Nama Domain Tidak Sesuai!'
        }
      });
    }
  }

  async handleBuy(productId, selectedIndex) {
    const {
      domain_search,
      user,
      appendDomainBuyFormData,
      pushState,
      loadUidProfile,
      changeDomainBooked,
      cookies
    } = this.props;

    this.setState({
      buy_process: true
    });

    const product = domain_search.data[selectedIndex];

    // Handle for unauthenticated user
    if (!user) {
      const data = {
        domain_name: product.name,
        product_id: productId
      };

      cookies.set('pending_book_domain', true, { path: '/' });
      window.localStorage.setItem('booked_domain_product', JSON.stringify(product));
      window.localStorage.setItem('booked_domain_data', JSON.stringify(data));

      this.setState({
        buy_process: false
      });
      pushState('/login');
    } else {
      await loadUidProfile(user.token);
      const { uid_profile } = this.props;
      const data = {
        domain_name: product.name,
        product_id: productId,
        name: uid_profile.full_name,
        state: uid_profile.address_ktp.province,
        city: uid_profile.address_ktp.city,
        address: uid_profile.address_ktp.road,
        email: uid_profile.email,
        phone: uid_profile.phone.replace(/\D/g, '')
      };
      appendDomainBuyFormData(data);
      changeDomainBooked(product);
      this.setState({
        buy_process: false
      });
      pushState('/registrar/domain/buy');
    }
  }

  render() {
    const { domain_products, domain_search_form, domain_search } = this.props;
    return (
      <div className={styles.search_container}>
        {domain_products.loaded ? (
          <Form onSubmit={this.handleSubmit}>
            <div className="row">
              <div className="col-lg-10 col-xs-8">
                <FormGroup
                  controlId="domainSearchForm"
                >
                  <InputGroup>
                    <FormControl
                      type="text"
                      name="domain_name"
                      placeholder="Nama Domain"
                      onChange={this.handleChange}
                      className={styles.input_domain_field}
                      autoComplete="off"
                    />
                    <DropdownButton
                      componentClass={InputGroup.Button}
                      className={styles.extension_dropdown}
                      id="input-dropdown-addon"
                      title={domain_search_form.extension}
                    >
                      {domain_products.data.map(product => (
                        <MenuItem
                          key={product.id}
                          onClick={() => { this.handleExtensionChange(product.name); }}
                        >
                          {product.name}</MenuItem>
                      ))}
                    </DropdownButton>
                  </InputGroup>
                </FormGroup>
              </div>
              <div className="col-lg-2 col-xs-2">
                <FormGroup>
                  <Button
                    disabled={!domain_search_form.domain_name}
                    onClick={this.handleSubmit}
                    className={styles.btn_pandi_2}
                  >
                    <i className="fa fa-search" />{' '}Cari</Button>
                </FormGroup>
              </div>
            </div>
            {(domain_search_form.domain_name && this.state.is_invalid_domain.status) && (
              <center>
                <span
                  className={styles.invalid_domain}
                >
                  {this.state.is_invalid_domain.message}
                </span>
              </center>
            )}
          </Form>
        ) : (
          <div>
            <Loading />
          </div>
        )}
        {/* TODO: Move to component */}
        <div>
          {(domain_search.loaded && !domain_search.loading && !this.state.buy_process) ? (
            <div>
              <Table responsive>
                <thead>
                  <tr>
                    <th />
                    <th>Nama Domain</th>
                    <th>Harga</th>
                    <th />
                  </tr>
                </thead>
                <tbody className={styles.domain_table_body}>
                  {domain_search.data.map((domain, index) => (
                    <tr key={index}>
                      <td>{domain.is_available ? (
                        <i className={`${styles.available_icon} fa fa-check-circle`} />
                      ) : (
                        <i className={`${styles.unavailable_icon} fa fa-times-circle`} />
                      )}</td>
                      <td>
                        {domain.is_available ? (
                          <span className={`${styles.available_text}`}>{domain.name}</span>
                        ) : (
                          <span className={`${styles.unavailable_text}`}>{domain.name}</span>
                        )}
                      </td>
                      <td>
                        {domain.is_available ? (
                          <span>Rp. {domain.product.price.new.fmt}/tahun</span>
                        ) : (
                          <span className={`${styles.unavailable_price}`}>Tidak tersedia</span>
                        )}
                      </td>
                      <td>{domain.is_available && (
                        <Button
                          className={`btn ${styles.btn_pandi_2}`}
                          onClick={() => this.handleBuy(domain.product.id, index)}
                        >
                          <i className="fa fa-cart-plus" /> Beli
                        </Button>
                      )}</td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </div>
          ) : (
            (domain_search.loading || this.state.buy_process) && (
              <BulletList />
            )
          )}
        </div>
      </div>
    );
  }
}
