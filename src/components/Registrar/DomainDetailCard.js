import React, { Component } from 'react';
import { Panel } from 'react-bootstrap';
import { Loading } from '../../components';
import Moment from 'react-moment';
import 'moment/locale/id';

const styles = require('./Registrar.scss');

export default class DomainDetailCard extends Component {
  render() {
    const { domainDetail, item_added } = this.props;
    const { product } = domainDetail;
    const periodItems = [];

    for (let i = 1; i <= 10; i++) {
      periodItems.push(<option value={i}>{i} Tahun</option>);
    }

    return (
      domainDetail.id ? (
        <Panel className={styles.panel}>
          <Panel.Heading className={styles.panel_header}>Informasi Domain</Panel.Heading>
          <Panel.Body>
            <div>
              <div className={styles.detail_title}>Nama Domain:</div>
              <div className={styles.detail_value}>{domainDetail.name}</div>
            </div>
            <div>
              <div className={styles.detail_title}>Tanggal Pendaftaran:</div>
              <div className={styles.detail_value}>
                <Moment
                  locale="id"
                  format="D MMMM YYYY"
                >
                  {domainDetail.registry_register_date}
                </Moment>
              </div>
            </div>
            <div>
              <div className={styles.detail_title}>Tanggal Kadaluwarsa:</div>
              <div className={styles.detail_value}>
                <Moment
                  locale="id"
                  format="D MMMM YYYY"
                >
                  {domainDetail.registry_expire_date}
                </Moment>
              </div>
            </div>
          </Panel.Body>
        </Panel>
      ) : (
        <Loading />
      )
    );
  }
}
