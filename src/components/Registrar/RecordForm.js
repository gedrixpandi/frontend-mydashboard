import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Button } from 'react-bootstrap';

const styles = require('./Registrar.scss');

const Input = ({
  input,
  type,
  styles,
  placeHolder,
  addOn,
  additionalStyles,
  meta: {
    touched, error
  }
}) => (
  <div className={`form-group ${styles.mr_5} ${error && touched ? 'has-error' : ''}`}>
    {addOn ? (
      <span className="input-group">
        <input {...input} type={type} className={`form-control ${additionalStyles} ${styles.my_field} ${styles.my_form_inline}`} id={input.name} placeholder={placeHolder} />
        <span className={`input-group-addon ${styles.my_group_addon}`}>{addOn}</span>
      </span>
    ) : (
      <input {...input} type={type} className={`form-control ${styles.my_field} ${additionalStyles} ${styles.my_form_inline}`} id={input.name} placeholder={placeHolder} />
    )}
  </div>
);

const Select = ({
  input,
  styles,
  placeHolder,
  options,
  meta: {
    touched, error
  }
}) => (
  <div className={`form-group ${styles.mr_5} ${error && touched ? 'has-error' : ''}`}>
    <select {...input} placeholder={placeHolder} id={input.name} className={`form-control ${styles.my_form_inline}`}>
      {options.map((option, i) => (
        <option key={i} value={option}>{option}</option>
      ))}
    </select>
  </div>
);

@reduxForm({
  form: 'records'
})

export default class RecordForm extends Component {
  render() {
    const { handleSubmit, data: { values } } = this.props;
    return (
      <div className={styles.contact_form_container}>
        <form className="form-inline" onSubmit={handleSubmit}>
          <Field
            component={Input}
            type="text"
            name="host"
            label="Host"
            placeHolder="Host"
            styles={styles}
            addOn={`.${this.props.domainName}`}
            additionalStyles={styles.field_150}
          />
          <Field
            component={Select}
            type="text"
            name="type"
            label="Type"
            placeHolder="Type"
            styles={styles}
            options={['A', 'CNAME', 'MX', 'NS', 'SOA', 'TXT']}
          />
          <Field
            component={Input}
            type="text"
            name="ttl"
            label="TTL"
            placeHolder="TTL"
            styles={styles}
            additionalStyles={styles.field_50}
          />
          {values && values.type === 'MX' && (
            <Field
              component={Input}
              type="text"
              name="priority"
              label="Priority"
              placeHolder="Priority"
              styles={styles}
              additionalStyles={styles.field_50}
            />
          )}
          <Field
            component={Input}
            type="text"
            name="value"
            label="Value"
            placeHolder="Value"
            styles={styles}
          />
          <div className={`form-group ${styles.my_form_inline}`}>
            <Button disabled={this.props.disableSubmitButton} type="submit" className={`btn btn-sm ${styles.btn_pandi_sm} ${styles.ml_5}`}>{this.props.submitButton || 'Simpan'}</Button>
          </div>
        </form>
      </div>
    );
  }
}
