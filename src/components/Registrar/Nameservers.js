import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Panel,
  ListGroup,
  ListGroupItem,
  Label,
  ButtonGroup,
  Button,
  FormGroup,
  Radio
} from 'react-bootstrap';
import ReactLoading from 'react-loading';
import Moment from 'react-moment';
import 'moment/locale/id';

const styles = require('./Registrar.scss');

@connect(state => ({
  user: state.auth.user,
}))

export default class Nameservers extends Component {
  constructor(props) {
    super(props);

    this.state = {
      is_default_ns: true
    };
  }

  switchNs(e) {
    console.log(e.target.value);
    this.setState({
      is_default_ns: e.target.value
    });
  }

  render() {
    return (
      <div>
        <h3 className={styles.component_title}>Name servers</h3>
        <FormGroup>
          <Radio name="defaultNs" value onChange={this.switchNs.bind(this)}>
            Gunakan name servers My.ID
          </Radio>
          <Radio name="defaultNs" value={false} onChange={this.switchNs.bind(this)}>
            Gunakan name servers kustom
          </Radio>
        </FormGroup>
        <div>
          <div className={`${styles.ns_title} ${styles.ml_5}`}>Name Server</div>
          {this.state.is_default_ns ? (
            <table className="table table-borderless">
              <tbody>
                <tr>
                  <td>ns1.domain.id</td>
                </tr>
                <tr>
                  <td>ns2.domain.id</td>
                </tr>
              </tbody>
            </table>
          ) : (
            <div>Custom</div>
          )}
          <Button className={`btn-sm ${styles.btn_pandi_2}`}>Simpan</Button>
        </div>
      </div>
    );
  }
}
