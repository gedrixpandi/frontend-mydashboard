import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { Button } from 'react-bootstrap';
import contactValidation from './contactValidation';

const styles = require('./Registrar.scss');

const Input = ({
  input,
  label,
  type,
  styles,
  placeHolder,
  meta: {
    touched, error, dirty, active, visited, asyncValidating
  }
}) => (
  <div className={`form-group ${error && touched ? 'has-error' : ''}`}>
    <label htmlFor={input.name}>
      {label}
    </label>
    <div>
      <input {...input} type={type} className={`form-control ${styles.my_field}`} id={input.name} placeholder={placeHolder} />
      {error && touched && (
        <div className="help-block">{error}</div>
      )}
    </div>
  </div>
);

@reduxForm({
  form: 'contact',
  validate: contactValidation,
})

export default class ContactForm extends Component {
  render() {
    const { handleSubmit } = this.props;
    return (
      <div className={styles.contact_form_container}>
        <form onSubmit={handleSubmit}>
          <Field
            component={Input}
            type="text"
            name="name"
            label="Nama Lengkap"
            placeHolder="Nama Lengkap"
            styles={styles}
          />
          <Field
            component={Input}
            type="text"
            name="organization"
            label="Organisasi"
            placeHolder="Organisasi"
            styles={styles}
          />
          <Field
            component={Input}
            type="text"
            name="state"
            label="Provinsi"
            placeHolder="Provinsi"
            styles={styles}
          />
          <Field
            component={Input}
            type="text"
            name="city"
            label="Kota"
            placeHolder="Kota"
            styles={styles}
          />
          <Field
            component={Input}
            type="text"
            name="address"
            label="Alamat Lengkap"
            placeHolder="Alamat Lengkap"
            styles={styles}
          />
          <Field
            component={Input}
            type="text"
            name="phone"
            label="Nomor Telepon"
            placeHolder="Nomor Telepon"
            styles={styles}
          />
          <Field
            component={Input}
            type="text"
            name="email"
            label="Email"
            placeHolder="Email"
            styles={styles}
          />
          <div className={styles.fl_right}>
            {this.props.cancel && (
              <Button onClick={this.props.cancel} className={styles.btn_cancel}>{this.props.cancelText || 'Batal'}</Button>
            )}
            <Button type="submit" className={`btn ${styles.btn_pandi} ${styles.fl_right}`}>{this.props.submitButton || 'Simpan'}</Button>
          </div>
        </form>
      </div>
    );
  }
}
