import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Panel,
  ListGroup,
  ListGroupItem,
  Label,
  ButtonGroup,
  Button
} from 'react-bootstrap';
import Moment from 'react-moment';
import { loadMyDomain } from 'redux/modules/registrar/domain';
import { Loading } from './../../components';

const styles = require('./Registrar.scss');

@connect(state => ({
  user: state.auth.user,
  my_domains: state.domain.my_domains
}), { loadMyDomain })

export default class DomainLists extends Component {
  componentWillMount() {
    const { my_domains, user, loadMyDomain } = this.props;

    if (!my_domains.loaded) {
      loadMyDomain(user.token);
    }
  }

  render() {
    const { my_domains } = this.props;
    return (
      <div>
        <table className="table table-borderless">
          <thead>
            <tr className={styles.table_header}>
              <td>DOMAIN</td>
              <td className={styles.centering}>DNS</td>
              <td className={styles.centering}>PENGATURAN</td>
              <td className={styles.centering}>KADALUWARSA</td>
              <td />
            </tr>
          </thead>
          {my_domains.loaded && (
            <tbody>
              {my_domains.data.map(domain => (
                <tr key={domain.id} className={`${styles.body_data}`}>
                  <td>{domain.name}</td>
                  <td className={styles.centering}>
                    {domain.registry_expire_date && (
                      <Link to={`/registrar/domain/${domain.name}/dns`}><i className="fa fa-server" /></Link>
                    )}
                  </td>
                  <td className={styles.centering}>
                    {domain.registry_expire_date && (
                      <Link to={`/registrar/domain/${domain.name}/settings`}><i className="fa fa-cog" /></Link>
                    )}
                  </td>
                  <td className={styles.centering}>
                    {domain.remaining_exp}
                  </td>
                </tr>
              ))}
            </tbody>
          )}
        </table>
        {my_domains.loading && (
          <Loading />
        )}
      </div>
    );
  }
}
