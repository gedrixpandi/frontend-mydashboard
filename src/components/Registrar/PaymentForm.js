import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {
  Panel,
  ListGroup,
  ListGroupItem,
  Label,
  ButtonGroup,
  Button,
  ButtonToolbar,
  ToggleButtonGroup,
  ToggleButton
} from 'react-bootstrap';
import ReactLoading from 'react-loading';
import { push } from 'react-router-redux';
import { appendDomainBuyFormData } from 'redux/modules/registrar/domain';
import { Loading } from './../../components';

const styles = require('./Registrar.scss');

const DEFAULT_BANK_CODE = '014';

@connect(state => ({
  user: state.auth.user,
  domain_booked: state.domain.domain_booked
}), {
  appendDomainBuyFormData, pushState: push
})

export default class PaymentForm extends Component {
  constructor(props) {
    super(props);
    this.changePayment = this.changePayment.bind(this);
  }

  componentDidMount() {
    const { appendDomainBuyFormData } = this.props;
    const data = {
      bank_code: DEFAULT_BANK_CODE
    };
    appendDomainBuyFormData(data);
  }

  changePayment(e) {
    const { appendDomainBuyFormData } = this.props;
    const data = {
      bank_code: e.target.value
    };
    appendDomainBuyFormData(data);
  }

  render() {
    const { domain_booked } = this.props;
    const logo_bca = require('./../../theme/img/bca.png');
    const logo_mandiri = require('./../../theme/img/mandiri.png');
    const currencyFormatter = require('currency-formatter');
    return (
      <Panel className={styles.noborder}>
        <Panel.Body>
          <Panel>
            <Panel.Body>
              Total Harga: {domain_booked.product.price.new.fmt}/tahun
            </Panel.Body>
          </Panel>
          <Panel>
            <Panel.Heading>Metode Pembayaran</Panel.Heading>
            <Panel.Body>
              <ButtonToolbar>
                <ToggleButtonGroup type="radio" name="options" defaultValue={DEFAULT_BANK_CODE}>
                  <ToggleButton value="014"><img src={logo_bca} width="50" /> Transfer Bank BCA</ToggleButton>
                </ToggleButtonGroup>
              </ButtonToolbar>
            </Panel.Body>
          </Panel>
          {this.props.loading ? (
            <Loading />
          ) : (
            <Button onClick={this.props.onSubmit} className="btn btn-block btn-warning btn-lg">Bayar</Button>
          )}
        </Panel.Body>
      </Panel>
    );
  }
}
