import memoize from 'lru-memoize';
import { createValidator, required, maxLength, email, integer } from 'utils/validation';

const contactValidation = createValidator({
  name: [required],
  state: [required],
  address: [required],
  phone: [required, integer],
  email: [required, email]
});
export default memoize(10)(contactValidation);
