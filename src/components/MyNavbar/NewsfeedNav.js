import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Nav,
  NavItem
} from 'react-bootstrap';
import { load as loadNewsfeed, loadFollowing as loadFollowingNewsfeed } from 'redux/modules/newsfeed';

const styles = require('./NewsfeedNav.scss');

@connect(state => ({
  user: state.auth.user,
  newsfeeds: state.newsfeed
}), { loadNewsfeed, loadFollowingNewsfeed })

class NewsfeedNav extends Component {
  constructor(props) {
    super(props);

    this.state = {
      active: 'all'
    };
  }

  async handleSelect(e) {
    const { user, loadNewsfeed, loadFollowingNewsfeed } = this.props;

    switch (e) {
      case 'all':
        loadNewsfeed(user.token);
        break;
      case 'following':
        loadFollowingNewsfeed(user.token);
        break;
    }

    this.setState({
      active: e
    });
  }

  render() {
    return (
      <Nav
        bsStyle="pills"
        className={`${styles.container} navbar_newsfeed`}
        activeKey={this.state.active}
        onSelect={this.handleSelect.bind(this)}
      >
        <NavItem className={styles.item} eventKey="all">
          Semua Tulisan
        </NavItem>
        <NavItem className={styles.item} eventKey="following">
          Tulisan yang Saya Ikuti
        </NavItem>
      </Nav>
    );
  }
}

export default NewsfeedNav;
