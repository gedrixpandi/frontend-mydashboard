import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { withRouter } from 'react-router';
import { logout } from 'redux/modules/auth';
import { Navbar, Nav, Tooltip, MenuItem, NavItem, Badge, OverlayTrigger } from 'react-bootstrap';
import { LinkContainer, IndexLinkContainer } from 'react-router-bootstrap';

@connect(
  state => ({
    user: state.auth.user,
    invoice_count: state.invoice.count,
    active_order: state.order.active
  }),
  { logout, pushState: push }
)
@withRouter
class MyNavbar extends Component {
  async handleLogout(e) {
    e.preventDefault();
    await this.props.logout();
    this.props.loadAuth();
  }

  tooltip(name) {
    return (
      <Tooltip id="tooltip">
        <strong>{name}</strong>
      </Tooltip>
    );
  }

  render() {
    const styles = require('./MyNavbar.scss');
    const logoImage = require('./logo_my_id.png');
    const { user, invoice_count, active_order } = this.props;
    const activeOrderCount = active_order.data && active_order.data.order_details.length;
    const unpaidInvoiceCount = invoice_count.data.unpaid_count;
    return (
      <Navbar collapseOnSelect fixedTop className={styles.navbar}>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="/">
              <img src={logoImage} width="115" />
            </Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <IndexLinkContainer to="/">
              <NavItem eventKey={1}>Beranda</NavItem>
            </IndexLinkContainer>
            <LinkContainer to="/registrar">
              <NavItem eventKey={4}>
                MyDomain <Badge>BETA</Badge>
              </NavItem>
            </LinkContainer>
          </Nav>
          {user && (
            <Nav pullRight>
              <LinkContainer to="/cart">
                <MenuItem eventKey={5.1} className={styles.menu_item_icon}>
                  <OverlayTrigger placement="bottom" overlay={this.tooltip('Keranjang')}>
                    <i className="fa fa-shopping-cart" />
                  </OverlayTrigger>
                  {activeOrderCount > 0 && (
                    <Badge className={styles.badge_red}>{activeOrderCount}</Badge>
                  )}
                </MenuItem>
              </LinkContainer>
              <LinkContainer to="/invoices">
                <MenuItem eventKey={5.2} className={styles.menu_item_icon}>
                  <OverlayTrigger placement="bottom" overlay={this.tooltip('Tagihan')}>
                    <i className="fa fa-file" />
                  </OverlayTrigger>
                  {unpaidInvoiceCount > 0 && (
                    <Badge className={styles.badge_red}>{unpaidInvoiceCount}</Badge>
                  )}
                </MenuItem>
              </LinkContainer>
              <MenuItem divider />
              <MenuItem
                eventKey={5.3}
                onClick={this.handleLogout.bind(this)}
                className={styles.menu_item_icon}
              >
                <OverlayTrigger placement="bottom" overlay={this.tooltip('Keluar')}>
                  <i className="fa fa-sign-out" />
                </OverlayTrigger>
              </MenuItem>
            </Nav>
          )}
        </Navbar.Collapse>
      </Navbar>
    );
  }
}

export default MyNavbar;
