import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { CounterButton, GithubButton } from 'components';
import config from 'config';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import {
  MyNavbar,
  SubscriptionCard,
  ExtendSubscription
} from './../../components';
import { Panel } from 'react-bootstrap';

@connect(state => ({
  online: state.online
}))
export default class NotFound extends Component {
  static propTypes = {
    online: PropTypes.bool.isRequired
  };

  render() {
    const { online } = this.props;
    // require the logo image both from client and server
    return (
      <div>
        <Helmet title="Halaman Tidak Ditemukan!" />
        <MyNavbar />
        <div className="container top-container">
          <div className="row">
            <div className="col-lg-12">
              <h1>Ups!</h1>
              <p>Halaman yang anda cari tidak ditemukan!</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
