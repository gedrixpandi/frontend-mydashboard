import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { push } from 'react-router-redux';
import { renderRoutes } from 'react-router-config';
import { provideHooks } from 'redial';
import Helmet from 'react-helmet';
import { isLoaded as isAuthLoaded, load as loadAuth, logout } from 'redux/modules/auth';
import { isLoaded as isSubscriptionLoaded, load as loadSubscription } from 'redux/modules/subscription';
import { count as countInvoice } from 'redux/modules/invoice';
import { isLoaded as isOrderLoaded, load as loadOrder } from 'redux/modules/order';
import { withCookies } from 'react-cookie';
import config from 'config';

@provideHooks({
  fetch: async ({ store: { dispatch, getState }, cookies }) => {
    if (cookies && cookies.mendoan) {
      if (!isAuthLoaded(getState())) {
        await dispatch(loadAuth(cookies.mendoan)).catch(() => null);
      }
      if (!isSubscriptionLoaded(getState())) {
        await dispatch(loadSubscription(cookies.mendoan)).catch(() => null);
      }
      if (!isOrderLoaded(getState())) {
        await dispatch(loadOrder(cookies.mendoan)).catch(() => null);
      }
      await dispatch(countInvoice(cookies.mendoan)).catch(() => null);
    }
  }
})
@connect(
  state => ({
    user: state.auth.user,
    blog: state.blog
  }),
  { logout, pushState: push }
)
@withRouter
@withCookies
export default class App extends Component {
  static propTypes = {
    route: PropTypes.objectOf(PropTypes.any).isRequired,
    location: PropTypes.objectOf(PropTypes.any).isRequired,
    user: PropTypes.shape({
      email: PropTypes.string
    }),
    logout: PropTypes.func.isRequired,
    pushState: PropTypes.func.isRequired
  };

  static defaultProps = {
    user: null
  };

  static contextTypes = {
    store: PropTypes.object.isRequired
  };

  componentWillMount() {
    console.log('Hi, did you find any bugs here? Please report to blog@pandi.id 😊');
  }

  componentWillReceiveProps(nextProps) {
    if (!this.props.user && nextProps.user) {
      // login
      const redirect = this.props.location.query && this.props.location.query.redirect;
      this.props.pushState(redirect || '/');
    } else if (this.props.user && !nextProps.user) {
      // logout
      this.props.pushState('/');
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.location !== prevProps.location) {
      window.scrollTo(0, 0);
    }
  }

  render() {
    const { user, notifs, route } = this.props;
    const styles = require('./App.scss');

    return (
      <div>
        <Helmet {...config.app.head} />
        {renderRoutes(route.routes)}
      </div>
    );
  }
}
