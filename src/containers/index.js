// export About from './About/About';
export App from './App/App';
// export Chat from './Chat/Chat';
// export ChatFeathers from './ChatFeathers/ChatFeathers';
export Home from './Home/Home';
// export Login from './Login/Login';
// export LoginSuccess from './LoginSuccess/LoginSuccess';
export NotFound from './NotFound/NotFound';
// export Register from './Register/Register';
// export Survey from './Survey/Survey';
// export Widgets from './Widgets/Widgets';

export Subscription from './Subscription/Subscription';

export Invoices from './Invoices/Invoices';
export InvoiceDetail from './Invoices/InvoiceDetail';

export Cart from './Cart/Cart';

export NewBlog from './Blogs/NewBlog';
export NewPremiumBlog from './Blogs/NewPremiumBlog';
export ManageBlog from './Blogs/Manage';
export BlogMXRecord from './Blogs/MXRecord';
export BlogAuth from './Blogs/BlogAuth';
export CustomDomain from './Blogs/CustomDomain';

export Registrar from './Registrar/Registrar';
export Dns from './Registrar/Dns';
export DomainSearch from './Registrar/Search';
export BuyDomain from './Registrar/BuyDomain';
export DomainSettings from './Registrar/DomainSettings';

export Profile from './Profile/Profile';
export EditProfile from './Profile/EditProfile';
