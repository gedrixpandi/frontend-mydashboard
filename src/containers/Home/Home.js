import React, { Component } from 'react';
import Helmet from 'react-helmet';
import {
  MyNavbar,
  MyBlogs,
  ProfileCard,
  NewsFeed,
  Footer,
  NewsfeedNav,
  MyBlogsSlider
} from './../../components';
import { AutoAffix } from 'react-overlays';
import { provideHooks } from 'redial';
import { isLoaded as isBlogLoaded, load as loadBlog } from 'redux/modules/blog';

const styles = require('./Home.scss');

@provideHooks({
  fetch: async ({ store: { dispatch, getState }, cookies }) => {
    if (cookies && cookies.mendoan) {
      if (!isBlogLoaded(getState())) {
        await dispatch(loadBlog(cookies.mendoan)).catch(() => null);
      }
    }
  }
})

export default class Home extends Component {
  render() {
    return (
      <div className={styles.home}>
        <Helmet title="My.ID - Platform Blog Indonesia" titleTemplate="" />
        <MyNavbar />
        <div className="container top-container">
          <div className="row">
            <div className="col-lg-3 col-md-3 col-sm-3 hidden-xs">
              <AutoAffix viewportOffsetTop={15} container={this}>
                <div>
                  <MyBlogs withCreateButton />
                </div>
              </AutoAffix>
            </div>
            <div className="col-lg-6 col-md-6 col-sm-6 hidden-lg hidden-md">
              <MyBlogsSlider withCreateButton />
            </div>
            <div className="col-lg-6 col-md-6 col-sm-6">
              <NewsfeedNav />
              <NewsFeed />
            </div>
            <div className="col-lg-3 col-md-3 col-sm-3 hidden-xs">
              <AutoAffix viewportOffsetTop={15} container={this}>
                <div>
                  <ProfileCard />
                  <Footer />
                </div>
              </AutoAffix>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
