import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import {
  MyNavbar,
  InvoiceTable
} from './../../components';
import {
  Panel,
  Label
} from 'react-bootstrap';

export default class Invoices extends Component {
  render() {
    const styles = require('./Invoices.scss');
    // require the logo image both from client and server
    return (
      <div>
        <Helmet title="Tagihan" />
        <MyNavbar />
        <div className="container top-container">
          <div className="row">
            <div className="col-lg-8 col-lg-offset-2">
              <InvoiceTable />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
