import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { MyNavbar } from './../../components';
import { Panel, Label, Button } from 'react-bootstrap';
import { load as loadInvoice, loadDetail, pay, count as countInvoice } from 'redux/modules/invoice';
import ReactLoading from 'react-loading';
import Moment from 'react-moment';
import 'moment/locale/id';
import { provideHooks } from 'redial';

const styles = require('./Invoices.scss');
@provideHooks({
  fetch: async ({ store: { dispatch, getState }, params: { invoice_id }, cookies }) => {
    if (!getState().invoice.detail.data && cookies) {
      await dispatch(loadDetail(invoice_id, cookies.mendoan)).catch(() => null);
    }
  }
})
@connect(
  state => ({
    user: state.auth.user,
    invoice_detail: state.invoice.detail
  }),
  {
    loadInvoice,
    loadDetail,
    pay,
    countInvoice
  }
)
export default class Invoices extends Component {
  constructor(props) {
    super(props);
    this.payConfirm = this.payConfirm.bind(this);
  }
  componentWillMount() {
    const {
      user, loadDetail, match, invoice_detail
    } = this.props;
    const invoiceId = match.params.invoice_id;

    if (!invoice_detail.loaded || invoice_detail.data.id !== invoiceId) {
      loadDetail(invoiceId, user.token);
    }
  }

  async payConfirm() {
    const {
      user, countInvoice, loadDetail, pay, match, loadInvoice
    } = this.props;
    const invoiceId = match.params.invoice_id;
    await pay(invoiceId, user.token);
    loadInvoice(user.token);
    countInvoice(user.token);
    loadDetail(invoiceId, user.token);
  }

  render() {
    const { invoice_detail } = this.props;
    return (
      <div>
        <MyNavbar />
        <div className="container top-container">
          <div className="row">
            <div className="col-lg-6 col-lg-offset-3">
              {invoice_detail.loaded &&
                !invoice_detail.loading && (
                <div>
                  <Helmet title={`Tagihan #${invoice_detail.data.invoice_number}`} />
                  <Panel>
                    <Panel.Heading className={styles.header}>
                        Informasi tagihan #{invoice_detail.data.invoice_number}
                    </Panel.Heading>
                    <Panel.Body>
                      <center>
                        <Label bsStyle={invoice_detail.data.payment_status.style}>
                          {invoice_detail.data.payment_status.text}
                        </Label>
                      </center>
                      <div className="row">
                        <div className={`col-lg-6 ${styles.div_detail}`}>
                          <h4>Metode Pembayaran</h4>
                          <img
                            className={styles.bank_logo}
                            src={invoice_detail.data.payment_method.image_url}
                            width="100"
                            alt="Bank Logo"
                          />
                          <p className={styles.bank_account_detail}>
                              No. Rek: {invoice_detail.data.payment_method.account_number}
                            <br />
                              a/n {invoice_detail.data.payment_method.account_name}
                          </p>
                        </div>

                        <div className={`col-lg-6 ${styles.div_detail}`}>
                          <h4>Total Tagihan</h4>
                          <h4 className={styles.amount}>
                              Rp{' '}
                            {
                              <span>
                                {invoice_detail.data.amount.fmt.substring(
                                  0,
                                  invoice_detail.data.amount.fmt.length - 3
                                )}
                                <span className={styles.unique_container}>
                                  {invoice_detail.data.amount.fmt.substring(invoice_detail.data.amount.fmt.length - 3)}
                                </span>
                              </span>
                            }
                          </h4>
                          <p className={`text-muted ${styles.force_left}`}>
                              * Untuk memudahkan proses verifikasi, pastikan transfer tepat hingga 3 digit terakhir.
                          </p>
                        </div>
                      </div>
                      {invoice_detail.data.payment_status.code === 0 && (
                        <div>
                          <p className={styles.due_date_text}>
                            Segera lakukan pembayaran untuk menyelesaikan transaksimu sebelum:<br />
                            <b>
                              <Moment locale="id" format="D MMMM YYYY">
                                {invoice_detail.data.due_date}
                              </Moment>
                            </b>
                          </p>
                          <Button
                            className={`${styles.btn_pay_confirmation} btn btn-danger btn-block`}
                            onClick={this.payConfirm}
                          >
                            <i className="fa fa-check" /> Saya Sudah Bayar
                          </Button>
                        </div>
                      )}
                      <Panel>
                        <Panel.Heading className={styles.header}>Detail Pemesanan</Panel.Heading>
                        <table className="table">
                          <tbody>
                            {invoice_detail.data.order.items.map(item => (
                              <tr key={item.id}>
                                <td>{item.product_name}</td>
                                <td>{item.qty}x</td>
                                <td>Rp {item.price.fmt}</td>
                              </tr>
                            ))}
                            <tr>
                              <td colSpan="2">PPN 10%</td>
                              <td>{invoice_detail.data.tax}</td>
                            </tr>
                            <tr>
                              <td colSpan="2">Kode Unik</td>
                              <td>{invoice_detail.data.unique_number}</td>
                            </tr>
                            <tr>
                              <td colSpan="2">Total Tagihan</td>
                              <td>
                                <b>Rp {invoice_detail.data.amount.fmt}</b>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </Panel>
                      <Link to="/invoices" className={`${styles.btn_back}`}>
                          Kembali
                      </Link>
                    </Panel.Body>
                  </Panel>
                </div>
              )}

              {invoice_detail.loading && (
                <ReactLoading type="bars" color="red" delay={0} className={styles.loading_container} />
              )}

              {invoice_detail.loaded &&
                !invoice_detail.data && (
                <div>
                  <Helmet title="Tagihan Tidak Ditemukan" />
                  <Panel>
                    <Panel.Heading className={styles.header}>Ups! Tagihan tidak ditemukan.</Panel.Heading>
                    <Panel.Body>
                      <Link to="/invoices" className={`${styles.btn_back} btn btn-default btn-block`}>
                          Kembali
                      </Link>
                    </Panel.Body>
                  </Panel>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
