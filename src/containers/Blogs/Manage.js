import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import {
  MyNavbar,
  MyBlogs,
  CreateBlog
} from './../../components';
import { Panel, Button } from 'react-bootstrap';
import { loadDetail } from 'redux/modules/blog';
import { Loading } from './../../components';

const styles = require('./Blogs.scss');

@connect(state => ({
  user: state.auth.user,
  subscription: state.subscription,
  blog_detail: state.blog.detail
}), { loadDetail })

export default class NewBlog extends Component {
  componentDidMount() {
    const { match, user, loadDetail } = this.props;
    loadDetail(match.params.blog_domain, user.token);
  }

  render() {
    const { blog_detail, subscription, user } = this.props;
    // require the logo image both from client and server
    return (
      <div>
        <MyNavbar />
        <div className="container top-container">
          <div className="row">
            <div className="col-lg-6 col-lg-offset-3" >
              {blog_detail.loaded && !blog_detail.loading ? (
                <div>
                  <Helmet title={blog_detail.data.blogname} />
                  <Panel className={styles.panel}>
                    <Panel.Heading className={styles.panel_header}>
                      <Link to="/" className="btn btn-default btn-xs"><i className="fa fa-chevron-left" /></Link>
                      {' '}
                      {blog_detail.data.alias ? `${blog_detail.data.alias} (${blog_detail.data.domain})` : blog_detail.data.domain}
                    </Panel.Heading>
                    <Panel.Body>
                      <Panel>
                        <Panel.Heading className={styles.panel_header}>
                          <i className="fa fa-user" /> Administrator Blog
                        </Panel.Heading>
                        <Panel.Body>
                          <p>Buat postingan, tambahkan halaman, atur tema, untuk {blog_detail.data.domain} di sini.</p>
                          <a href={`http://${blog_detail.data.domain}/cb.php?access_token=${user.uid_access_token}`} className={`btn btn-sm ${styles.btn_admin} ${styles.float_right}`}>
                              Kunjungi Halaman Administrator <i className="fa fa-angle-right" />
                          </a>
                        </Panel.Body>
                      </Panel>
                      <Panel>
                        <Panel.Heading className={styles.panel_header}>
                          <i className="fa fa-envelope-open" /> Manajemen MX Record
                        </Panel.Heading>
                        <Panel.Body>
                          <p>MX Record adalah tipe record yang berfungsi untuk menunjukkan lokasi server email. Dengan menggunakan fitur ini kamu dapat menentukan tujuan server email @{blog_detail.data.domain}.</p>
                          <Link to={`/blogs/${blog_detail.data.domain}/mx`} className={`btn btn-sm ${styles.btn_admin} ${styles.float_right}`}>
                              Atur MX Record <i className="fa fa-angle-right" />
                          </Link>
                        </Panel.Body>
                      </Panel>
                      <Panel className={subscription.data.status !== 'Premium' ? styles.panel_disabled : ''}>
                        <Panel.Heading className={styles.panel_header}>
                          <i className="fa fa-certificate" /> Kustomisasi Domain (Premium)
                        </Panel.Heading>
                        <Panel.Body>
                          <p>Kamu bisa menggunakan domain sendiri untuk blog My.ID mu! Arahkan domainmu ke server My.ID dan atur domain utaman blogmu.</p>
                          <Link to={`/blogs/${blog_detail.data.domain}/domain`} className={`btn btn-sm ${styles.btn_admin} ${styles.float_right}`}>
                              Atur Domain <i className="fa fa-angle-right" />
                          </Link>
                        </Panel.Body>
                      </Panel>
                    </Panel.Body>
                  </Panel>
                </div>
              ) : (
                blog_detail.loading ? (
                  <Loading />
                ) : (
                  <Panel className={styles.panel}>
                    <Panel.Heading className={styles.panel_header}>Blog Tidak Ditemukan</Panel.Heading>
                    <Panel.Body>
                      <Link className="btn btn-danger btn-block" to="/">Kembali</Link>
                    </Panel.Body>
                  </Panel>
                )
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
