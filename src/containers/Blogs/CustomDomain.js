import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import {
  MyNavbar,
  SetupCustomDomain,
  CustomedDomain
} from './../../components';
import {
  Panel,
  Button,
  Form,
  FormGroup,
  FormControl,
  ControlLabel,
  Alert,
  InputGroup
} from 'react-bootstrap';
import { loadDetail, loadMx, deleteMx } from 'redux/modules/blog';
import { create as createRecord } from 'redux/modules/registrar/record';
import { Loading } from './../../components';

const styles = require('./Blogs.scss');

@connect(state => ({
  user: state.auth.user,
  subscription: state.subscription,
  blog_detail: state.blog.detail,
  blog_mx: state.blog.mx,
  record_create: state.record.create
}), {
  loadDetail, loadMx, deleteMx, createRecord
})

export default class CustomDomain extends Component {
  constructor(props) {
    super(props);

    this.state = {
      step: 1
    };
  }

  componentDidMount() {
    this.initiate();
  }

  async initiate() {
    const {
      match, user, loadDetail
    } = this.props;
    const blogDetail = await loadDetail(match.params.blog_domain, user.token);
  }

  render() {
    const {
      blog_detail, blog_mx, subscription, record_create
    } = this.props;
    // require the logo image both from client and server
    return (
      <div>
        <MyNavbar />
        <div className="container top-container">
          <div className="row">
            <div className="col-lg-8 col-lg-offset-2" >
              {blog_detail.loaded && !blog_detail.loading ? (
                <div>
                  <Helmet title="Kustomisasi Domain" />
                  <Panel className={styles.panel}>
                    <Panel.Heading className={styles.panel_header}>
                      <Link to={`/blogs/${blog_detail.data.domain}`} className="btn btn-default btn-xs"><i className="fa fa-chevron-left" /></Link> <i className="fa fa-certificate" /> Kustomisasi Domain
                    </Panel.Heading>
                    <Panel.Body>
                      {!blog_detail.data.alias ? (
                        <SetupCustomDomain />
                      ) : (
                        <CustomedDomain />
                      )}
                    </Panel.Body>
                  </Panel>
                </div>
              ) : (
                blog_detail.loading ? (
                  <Loading />
                ) : (
                  <Panel className={styles.panel}>
                    <Panel.Heading className={styles.panel_header}>Blog Tidak Ditemukan</Panel.Heading>
                    <Panel.Body>
                      <Link className="btn btn-danger btn-block" to="/">Kembali</Link>
                    </Panel.Body>
                  </Panel>
                )
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
