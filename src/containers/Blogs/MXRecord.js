import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import {
  MyNavbar,
  MyBlogs,
  CreateBlog
} from './../../components';
import { Panel, Button, Form, FormGroup, FormControl, ControlLabel } from 'react-bootstrap';
import { loadDetail, loadMx, deleteMx } from 'redux/modules/blog';
import { create as createRecord } from 'redux/modules/registrar/record';
import { Loading } from './../../components';

const styles = require('./Blogs.scss');

@connect(state => ({
  user: state.auth.user,
  subscription: state.subscription,
  blog_detail: state.blog.detail,
  blog_mx: state.blog.mx,
  record_create: state.record.create
}), {
  loadDetail, loadMx, deleteMx, createRecord
})

export default class NewBlog extends Component {
  constructor(props) {
    super(props);

    this.state = {
      ttl: '',
      content: '',
      prio: ''
    };
  }

  componentDidMount() {
    this.initiateMx();
  }
  async initiateMx() {
    const {
      match, user, loadDetail, loadMx
    } = this.props;
    const blogDetail = await loadDetail(match.params.blog_domain, user.token);

    loadMx(this.props.blog_detail.data.domain, user.token);
  }

  async addRecord(e) {
    const {
      user, createRecord, blog_detail, loadMx
    } = this.props;
    e.preventDefault();

    const createdRecord = await createRecord(blog_detail.data.domain, {
      name: '@',
      type: 'MX',
      content: this.state.content,
      ttl: this.state.ttl,
      prio: this.state.prio,
    }, user.token);

    this.setState({
      ttl: '',
      content: '',
      prio: ''
    });

    loadMx(blog_detail.data.domain, user.token);
  }

  async deleteRecord(recordId, e) {
    const {
      blog_detail, deleteMx, user, loadMx
    } = this.props;

    const parentElement = e.target.parentNode;
    parentElement.disabled = true;
    parentElement.innerHTML = 'Menghapus...';

    const deletedMx = await deleteMx(blog_detail.data.domain, recordId, user.token);

    loadMx(blog_detail.data.domain, user.token);
  }

  handleFormChange(e) {
    if (e.target.value) {
      this.setState({
        [e.target.name]: e.target.value
      });
    } else {
      this.setState({
        [e.target.name]: ''
      });
    }
  }

  render() {
    const {
      blog_detail, blog_mx, subscription, record_create
    } = this.props;
    // require the logo image both from client and server
    return (
      <div>
        <MyNavbar />
        <div className="container top-container">
          <div className="row">
            <div className="col-lg-8 col-lg-offset-2" >
              {blog_detail.loaded && !blog_detail.loading ? (
                <div>
                  <Helmet title="Manajemen MX Record" />
                  <Panel className={styles.panel}>
                    <Panel.Heading className={styles.panel_header}>
                      <Link to={`/blogs/${blog_detail.data.domain}`} className="btn btn-default btn-xs"><i className="fa fa-chevron-left" /></Link> <i className="fa fa-envelope-open" /> Manajemen MX Record&nbsp;
                      <small className={styles.title_domain}>
                        ({blog_detail.data.domain})
                      </small>
                    </Panel.Heading>
                    <Panel.Body>
                      <Form inline className={`${styles.centering} ${styles.mb_10} ${record_create.loading ? styles.panel_disabled : ''}`}>
                        <FormGroup controlId="formInlineName">
                          <ControlLabel>@</ControlLabel>&nbsp;
                          <FormControl onChange={this.handleFormChange.bind(this)} name="ttl" className={styles.my_field} type="text" placeholder="Time to Live (TTL*)" value={this.state.ttl} />
                        </FormGroup>&nbsp;
                        <FormGroup controlId="formInlineName">
                          <FormControl onChange={this.handleFormChange.bind(this)} name="content" className={styles.my_field} type="text" placeholder="Target/Value" value={this.state.content} />
                        </FormGroup>&nbsp;
                        <FormGroup controlId="formInlineName">
                          <FormControl onChange={this.handleFormChange.bind(this)} name="prio" className={styles.my_field} componentClass="select" placeholder="Priority" defaultValue={this.state.prio}>
                            <option value="">Priority</option>
                            <option value="1">1</option>
                            <option value="5">5</option>
                            <option value="10">10</option>
                          </FormControl>
                        </FormGroup>&nbsp;
                        <Button
                          disabled={(!this.state.ttl || !this.state.content || !this.state.prio)}
                          type="submit"
                          onClick={this.addRecord.bind(this)}
                        >Tambahkan</Button>
                      </Form>
                      <Panel>
                        <Panel.Heading className={styles.panel_header}>
                          MX Records
                        </Panel.Heading>
                        <Panel.Body>
                          <table className={`table table-borderless ${styles.centering}`}>
                            <thead>
                              <tr>
                                <td>Name</td>
                                <td>Time to Live (TTL*)</td>
                                <td>Type</td>
                                <td>Priority</td>
                                <td>Value</td>
                                <td />
                              </tr>
                            </thead>
                            {blog_mx.loaded && !blog_mx.loading ? (
                              <tbody>
                                {blog_mx.data && blog_mx.data.map((mx, i) => (
                                  <tr key={i}>
                                    <td>{mx.name}</td>
                                    <td>{mx.ttl}</td>
                                    <td>{mx.type}</td>
                                    <td>{mx.prio}</td>
                                    <td>{mx.content}</td>
                                    <td>
                                      <Button
                                        className="btn btn-default btn-xs btn-danger"
                                        onClick={this.deleteRecord.bind(this, mx.id)}
                                      >
                                        <i className="fa fa-times" />
                                      </Button>
                                    </td>
                                  </tr>
                                ))}
                              </tbody>
                            ) : (
                              <tbody>
                                <tr>
                                  <td colSpan="6">
                                    Memuat data...
                                  </td>
                                </tr>
                              </tbody>
                            )}
                          </table>
                          <p>* TTL adalah jumlah detik sebelum perubahan selanjutnya pada data MX mulai berlaku.</p>
                        </Panel.Body>
                      </Panel>
                    </Panel.Body>
                  </Panel>
                </div>
              ) : (
                blog_detail.loading ? (
                  <Loading />
                ) : (
                  <Panel className={styles.panel}>
                    <Panel.Heading className={styles.panel_header}>Blog Tidak Ditemukan</Panel.Heading>
                    <Panel.Body>
                      <Link className="btn btn-danger btn-block" to="/">Kembali</Link>
                    </Panel.Body>
                  </Panel>
                )
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
