import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import {
  MyNavbar,
  CreateBlog
} from './../../components';

@connect(state => ({
  online: state.online
}))
export default class NewBlog extends Component {
  static propTypes = {
    online: PropTypes.bool.isRequired
  };

  render() {
    const styles = require('./Blogs.scss');
    // require the logo image both from client and server
    return (
      <div className={styles.home}>
        <Helmet title="Buat Blog" />
        <MyNavbar />
        <div className="container top-container">
          <div className="row">
            <div className="col-lg-8 col-lg-offset-2">
              <CreateBlog />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
