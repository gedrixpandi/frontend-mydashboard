import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import {
  MyNavbar,
  MyBlogs,
  CreateBlog
} from './../../components';
import { Panel, Button } from 'react-bootstrap';
import { loadDetail, authBlog } from 'redux/modules/blog';
import { Loading } from './../../components';

const styles = require('./Blogs.scss');

@connect(state => ({
  user: state.auth.user,
  subscription: state.subscription,
  blog_detail: state.blog.detail,
  blog_callback: state.blog.callback
}), { loadDetail, authBlog })

export default class NewBlog extends Component {
  componentDidMount() {
    this.go();
  }

  async go() {
    const {
      match, user, loadDetail, authBlog, blog_detail, blog_callback
    } = this.props;

    const blogDetail = await loadDetail(match.params.blog_domain, user.token);

    if (blog_detail.loaded && blog_detail.data.domain) {
      const auth = await authBlog(blog_detail.data.domain, user.uid_access_token);

      if (this.props.blog_callback.status === 'success') {
        console.log('Ini mah sukses coyyy!!!');
        // window.location = `http://${blog_detail.data.domain}/wp-admin/`;
      }
    }
  }

  render() {
    const { blog_detail, subscription } = this.props;
    // require the logo image both from client and server
    return (
      <div>
        <MyNavbar />
        <div className="container top-container">
          <div className="row">
            <div className="col-lg-6 col-lg-offset-3" >
              {blog_detail.loaded && !blog_detail.loading ? (
                <div>
                  <Helmet title={`Autentikasi ${blog_detail.data.blogname}...`} />
                  <Panel className={styles.panel}>
                    <Panel.Body>
                      <center>
                        <Loading />
                        <p>Proses Autentikasi. Mohon Tunggu....</p>
                      </center>
                    </Panel.Body>
                  </Panel>
                </div>
              ) : (
                blog_detail.loading ? (
                  <Loading />
                ) : (
                  <Panel className={styles.panel}>
                    <Panel.Heading className={styles.panel_header}>Blog Tidak Ditemukan</Panel.Heading>
                    <Panel.Body>
                      <Link className="btn btn-danger btn-block" to="/">Kembali</Link>
                    </Panel.Body>
                  </Panel>
                )
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
