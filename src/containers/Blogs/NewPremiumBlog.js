import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import {
  MyNavbar,
  CreateBlogPremium
} from './../../components';

@connect(state => ({
  online: state.online
}))
export default class NewPremiumBlog extends Component {
  static propTypes = {
    online: PropTypes.bool.isRequired
  };

  render() {
    const styles = require('./Blogs.scss');
    // require the logo image both from client and server
    return (
      <div className={styles.home}>
        <Helmet title="Buat Blog Premium" />
        <MyNavbar />
        <div className="container top-container">
          <div className="row">
            <div className="col-lg-8 col-lg-offset-2">
              <CreateBlogPremium />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
