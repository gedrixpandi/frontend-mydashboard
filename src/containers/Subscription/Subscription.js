import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { MyNavbar, SubscriptionCard, ExtendSubscription } from './../../components';
import { push } from 'react-router-redux';
import { connect } from 'react-redux';
@connect(
  null,
  {
    pushState: push
  }
)
export default class Subscription extends Component {
  componentDidMount() {
    const { pushState } = this.props;
    pushState('/');
  }

  render() {
    const styles = require('./Subscription.scss');
    // require the logo image both from client and server
    return (
      <div className={styles.home}>
        <Helmet title="Langganan Premium" />
        <MyNavbar />
        <div className="container top-container">
          <div className="row">
            <div className="col-lg-4">
              <SubscriptionCard />
            </div>
            <div className="col-lg-8">
              <ExtendSubscription />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
