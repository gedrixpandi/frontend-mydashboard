import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { withCookies } from 'react-cookie';
import {
  appendDomainBuyFormData,
  changeDomainBooked
} from 'redux/modules/registrar/domain';
import { loadUidProfile } from 'redux/modules/profile';
import cookie from 'js-cookie';
import { Loading } from './../../components';
import {
  MyNavbar,
  DomainInformationCard,
  RegistrarHeader
} from './../../components';

@connect(state => ({
  user: state.auth.user,
  uid_profile: state.profile.uid_profile.data,
  domain_buy_form: state.domain.domain_buy_form,
}), {
  appendDomainBuyFormData,
  loadUidProfile,
  changeDomainBooked
})
@withCookies

export default class BuyDomain extends Component {
  state = {
    savedData: {},
    savedProduct: {}
  };

  componentWillMount() {
    this.initiateBookedDomain();
  }

  async initiateBookedDomain() {
    const {
      cookies,
      appendDomainBuyFormData,
      loadUidProfile,
      changeDomainBooked,
      user
    } = this.props;

    if (cookies.get('pending_book_domain')) {
      this.setState({
        savedData: JSON.parse(window.localStorage.getItem('booked_domain_data')),
        savedProduct: JSON.parse(window.localStorage.getItem('booked_domain_product'))
      });
      cookie.remove('pending_book_domain');
      window.localStorage.removeItem('booked_domain_data');
      window.localStorage.removeItem('booked_domain_product');

      await loadUidProfile(user.token);

      const { uid_profile } = this.props;

      const newData = {
        ...this.state.savedData,
        name: uid_profile.full_name,
        state: uid_profile.address_ktp.province,
        city: uid_profile.address_ktp.city,
        address: uid_profile.address_ktp.road,
        email: uid_profile.email,
        phone: uid_profile.phone.replace(/\D/g, '')
      };

      appendDomainBuyFormData(newData);
      changeDomainBooked(this.state.savedProduct);

      this.setState({
        buy_process: false
      });
    }
  }

  render() {
    return (
      <div>
        <Helmet title="Informasi Pembelian Domain" />
        <MyNavbar />
        <div className="container top-container">
          <div className="row">
            <RegistrarHeader desc="Informasi Pembelian Domain" />
          </div>
          <div className="row">
            <div className="col-lg-8 col-lg-offset-2">
              {this.props.domain_buy_form.domain_name ? (
                <DomainInformationCard />
              ) : (
                <Loading />
              )}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
