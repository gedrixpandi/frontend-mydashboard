import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import {
  MyNavbar,
  DomainLists,
  RegistrarSideMenu,
  RegistrarHeader
} from './../../components';
import {
  Panel,
  Label
} from 'react-bootstrap';
import { provideHooks } from 'redial';
import { loadMyDomain } from 'redux/modules/registrar/domain';

const styles = require('./Registrar.scss');

@provideHooks({
  fetch: async ({ store: { dispatch, getState }, cookies }) => {
    if (cookies && cookies.mendoan) {
      if (!getState().domain.my_domains.loaded) {
        await dispatch(loadMyDomain(cookies.mendoan)).catch(() => null);
      }
    }
  }
})

export default class Registrar extends Component {
  render() {
    // require the logo image both from client and server
    return (
      <div>
        <Helmet title="Domain Saya" />
        <MyNavbar />
        <div className="container top-container">
          <div className="row">
            <RegistrarHeader desc="Cara Kekinian Beli Domain!" />
          </div>
          <div className="row">
            <div className="col-lg-2">
              <RegistrarSideMenu />
            </div>
            <div className="col-lg-8">
              <DomainLists />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
