import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { provideHooks } from 'redial';
import {
  MyNavbar,
  DomainSearch,
  Nameservers,
  RegistrarHeader
} from './../../components';
import {
  Panel,
  Label
} from 'react-bootstrap';
import { loadProduct, isProductLoaded } from 'redux/modules/registrar/domain';

const styles = require('./Registrar.scss');

@provideHooks({
  fetch: async ({ store: { dispatch, getState } }) => {
    if (!isProductLoaded(getState())) {
      await dispatch(loadProduct()).catch(() => null);
    }
  }
})

export default class Search extends Component {
  render() {
    // require the logo image both from client and server
    return (
      <div>
        <Helmet title="Cari Domain" />
        <MyNavbar />
        <div className="container top-container">
          <div className="row">
            <RegistrarHeader desc="Cari Domain" />
          </div>
          <div className="row">
            <div className="col-lg-8 col-lg-offset-2">
              <DomainSearch />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
