import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { provideHooks } from 'redial';
import {
  MyNavbar,
  RegistrarHeader,
  DomainRenewal,
  DomainDetailCard
} from './../../components';
import { loadDomainDetail } from 'redux/modules/registrar/domain';

const styles = require('./Registrar.scss');

@provideHooks({
  fetch: async ({ store: { dispatch, getState }, params: { domain }, cookies }) => {
    if (!getState().domain.domain_detail.loaded && cookies) {
      await dispatch(loadDomainDetail(cookies.mendoan, domain)).catch(() => null);
    }
  }
})

@connect(state => ({
  user: state.auth.user,
  domain_detail: state.domain.domain_detail
}), {
  loadDomainDetail
})

export default class DomainSettings extends Component {
  componentWillMount() {
    this.initiate();
  }

  async initiate() {
    const {
      user, match, loadDomainDetail, pushState, domain_detail
    } = this.props;
    if (domain_detail.data.name !== match.params.domain) {
      await loadDomainDetail(user.token, match.params.domain);
    }

    if (!this.props.domain_detail.data.id) {
      pushState('/registrar');
    }
  }

  render() {
    const { domain_detail } = this.props;
    return (
      <div>
        <Helmet title={`Pengaturan ${domain_detail.data.name}`} />
        <MyNavbar />
        <div className="container top-container">
          <div className="row">
            <RegistrarHeader desc={`Pengaturan ${domain_detail.data.name}`} />
          </div>
          <div className={`row ${styles.mt_5}`}>
            <div className="col-lg-4">
              <DomainDetailCard domainDetail={domain_detail.data} />
            </div>
            <div className="col-lg-4">
              <DomainRenewal domainDetail={domain_detail.data} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
