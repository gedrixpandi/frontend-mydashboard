import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { push } from 'react-router-redux';
import { loadDomainDetail } from 'redux/modules/registrar/domain';
import { update as updateNameserver } from 'redux/modules/registrar/nameserver';
import { create as createRecord, load as loadRecord } from 'redux/modules/registrar/record';
import { reset } from 'redux-form';
import {
  MyNavbar,
  NameServerForm,
  RegistrarHeader,
  Loading,
  RecordForm,
  RecordList
} from './../../components';
import {
  Panel,
  Alert
} from 'react-bootstrap';

const styles = require('./Registrar.scss');

@connect(state => ({
  user: state.auth.user,
  domain_detail: state.domain.domain_detail,
  ns_update: state.nameserver.update,
  record_create: state.record.create,
  record_form_data: state.form.records
}), {
  loadDomainDetail, updateNameserver, createRecord, loadRecord, pushState: push
})

export default class Dns extends Component {
  constructor(props) {
    super(props);
    this.submitNameserver = this.submitNameserver.bind(this);
    this.submitRecord = this.submitRecord.bind(this);
  }

  componentWillMount() {
    this.initiate();
  }

  async initiate() {
    const {
      user, match, loadDomainDetail, pushState
    } = this.props;
    await loadDomainDetail(user.token, match.params.domain);
    if (!this.props.domain_detail.data) {
      pushState('/registrar');
    }
  }

  async submitNameserver(data) {
    const {
      user, match, updateNameserver
    } = this.props;

    updateNameserver(match.params.domain, data.nameservers, user.token);
  }

  async submitRecord(data) {
    const {
      user, match, createRecord, loadRecord, dispatch
    } = this.props;

    let fixedHost = data.host ? `${data.host}.${match.params.domain}` : match.params.domain;

    if (fixedHost === `@.${match.params.domain}`) {
      fixedHost = '@';
    }

    await createRecord(match.params.domain, {
      name: fixedHost,
      type: data.type,
      content: data.value,
      ttl: data.ttl || 86400,
      prio: data.priority || null
    }, user.token);
    dispatch(reset('records'));
    loadRecord(match.params.domain, user.token);
  }

  render() {
    return (
      <div>
        <Helmet title="Manajemen DNS" />
        <MyNavbar />
        <div className="container top-container">
          <div className="row">
            <RegistrarHeader desc="Manajemen DNS" />
          </div>
          <div className={`row ${styles.mt_5}`}>
            <div className="col-lg-8 col-lg-offset-2">
              <Panel className={styles.panel}>
                <Panel.Heading className={styles.panel_header}>Name servers</Panel.Heading>
                {this.props.ns_update.loaded && (
                  <Alert bsStyle="success">
                    Name server berhasil disimpan
                  </Alert>
                )}
                <Panel.Body>
                  {this.props.domain_detail.loaded && (
                    <NameServerForm
                      initialValues={{ nameservers: this.props.domain_detail.data.fmt_ns }}
                      onSubmit={this.submitNameserver}
                      submitButton={this.props.ns_update.loading ? 'Mohon Tunggu...' : 'Simpan'}
                      disableSubmitButton={this.props.ns_update.loading}
                    />
                  )}
                  {this.props.domain_detail.loading && (
                    <Loading />
                  )}
                </Panel.Body>
              </Panel>
            </div>
          </div>
          <div className={`row ${styles.mt_5}`}>
            <div className="col-lg-8 col-lg-offset-2">
              <Panel className={styles.panel}>
                <Panel.Heading className={styles.panel_header}>Records</Panel.Heading>
                {this.props.record_create.loaded && (
                  <Alert bsStyle="success">
                    Record berhasil disimpan
                  </Alert>
                )}
                <Panel.Body>
                  <RecordForm
                    domainName={this.props.match.params.domain}
                    onSubmit={this.submitRecord}
                    submitButton={this.props.record_create.loading ? 'Mohon Tunggu...' : 'Simpan'}
                    disableSubmitButton={this.props.record_create.loading}
                    initialValues={{ type: 'A' }}
                    data={this.props.record_form_data || {}}
                  />
                  <RecordList domainName={this.props.match.params.domain} />
                </Panel.Body>
              </Panel>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
