import React, { Component } from 'react';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { MyNavbar, CartList, CartSummaryInfo, CartPayment } from './../../components';
import { load } from 'redux/modules/order';
import { provideHooks } from 'redial';

const styles = require('./Cart.scss');

@provideHooks({
  fetch: async ({ store: { dispatch, getState }, cookies }) => {
    if (!getState().order.active.loaded && cookies) {
      await dispatch(load(cookies.mendoan)).catch(() => null);
    }
  }
})
@connect(
  state => ({
    user: state.auth.user,
    active_order: state.order.active
  }),
  {
    load
  }
)
export default class Cart extends Component {
  componentWillMount() {
    const { load, user } = this.props;
    load(user.token);
  }
  render() {
    const { active_order } = this.props;
    return (
      <div>
        <Helmet title="Keranjang" />
        <MyNavbar />
        <div className="container top-container">
          {active_order.data.order_details.length ? (
            <div className="row">
              <div className="col-lg-8">
                <CartList orderDetails={active_order.loaded && active_order.data.order_details} />
              </div>
              <div className="col-lg-4">
                <CartSummaryInfo activeOrder={active_order.loaded && active_order.data} />
                <CartPayment />
              </div>
            </div>
          ) : (
            <div className="row">
              <div className="col-lg-8 col-lg-offset-2">
                <p className={styles.text_centered}>Kamu belum memiliki pesanan apa pun di sini.</p>
              </div>
            </div>
          )}
        </div>
      </div>
    );
  }
}
