import React, { Component } from 'react';
import { PropTypes, instanceOf } from 'prop-types';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import LoginForm from 'components/LoginForm/LoginForm';
import FacebookLogin from 'components/FacebookLogin/FacebookLogin';
import * as authActions from 'redux/modules/auth';
import * as notifActions from 'redux/modules/notifs';
import { Link } from 'react-router-dom';
import { Footer } from './../../components';
import config from 'config';
import { withCookies, Cookies } from 'react-cookie';
import { Alert } from 'react-bootstrap';

@connect(state => ({ user: state.auth.user }), { ...notifActions, ...authActions })
@withCookies
export default class Login extends Component {
  static propTypes = {
    user: PropTypes.shape({
      email: PropTypes.string
    }),
    login: PropTypes.func.isRequired,
    logout: PropTypes.func.isRequired,
    notifSend: PropTypes.func.isRequired
  };

  static defaultProps = {
    user: null
  };

  static contextTypes = {
    router: PropTypes.object
  };

  onFacebookLogin = async (err, data) => {
    if (err) return;

    try {
      await this.props.login('facebook', data);
      this.successLogin();
    } catch (error) {
      if (error.message === 'Incomplete oauth registration') {
        this.context.router.push({
          pathname: '/register',
          state: { oauth: error.data }
        });
      } else {
        throw error;
      }
    }
  };

  login = async data => {
    const result = await this.props.login('local', data);
    this.successLogin();
    return result;
  };

  successLogin = () => {
    this.props.notifSend({
      message: "You're logged !",
      kind: 'success',
      dismissAfter: 2000
    });
  };

  FacebookLoginButton = ({ facebookLogin }) => (
    <button className="btn btn-primary" onClick={facebookLogin}>
      Login with <i className="fa fa-facebook-f" />
    </button>
  );

  render() {
    const { user, logout, cookies } = this.props;
    const styles = require('./Login.scss');
    const logoImage = require('./logo_my_id.png');
    return (
      <section>
        <Helmet title="Masuk" />
        <div className="login-container">
          <div className="row bg">
            <div className="col-md-4 col-md-offset-8 col-xs-12 login-sec">
              <div className="login-logo">
                <a href="https://www.my.id">
                  <img src={logoImage} />
                </a>
                {cookies.get('error_message') && (
                  <div className={styles.error_box}>
                    <strong>Ups...</strong>
                    <ul>
                      {cookies.get('error_message').map(message =>
                        <li>{message}</li>)}
                    </ul>
                    <p align="center">Silakan lengkapi profil u.id kamu <a href="https://u.id/pengguna/profil">di sini</a></p>
                  </div>
                )}
              </div>
              <div className="login-button">
                <p className={styles.intro_text}>
              Masuk dan terhubung dengan pengguna My.ID lainnya.
              Dapatkan kemudahan dalam mengelola blog Kamu.
                </p>
                <a href={config.uid_login} className="btn btn-default btn-uid">Masuk dengan akun </a>
                <div className="register-text">
                  <a href="https://u.id/register" target="_blank" className={`${styles.register_button} btn`} to="register">SAYA BELUM PUNYA AKUN U.ID</a>
                </div>
              </div>
              <div className="copyright-text">
                <Footer />
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
