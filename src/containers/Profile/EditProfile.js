import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { ProfileEditor, MyNavbar } from './../../components';
import {
  Panel,
  Label
} from 'react-bootstrap';

const styles = require('./Profile.scss');

@connect(state => ({
  user: state.auth.user
}), {})

export default class EditProfile extends Component {
  render() {
    const { user } = this.props;
    // require the logo image both from client and server
    return (
      <div>
        <Helmet title={`${user.full_name}`} />
        <MyNavbar />
        <div className="container top-container">
          <div className="row">
            <div className="col-lg-6 col-lg-offset-3">
              <ProfileEditor />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
