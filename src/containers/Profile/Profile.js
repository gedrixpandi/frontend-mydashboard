import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';
import { connect } from 'react-redux';
import { DynamicProfileCard, MyNavbar } from './../../components';
import { loadProfile } from 'redux/modules/profile';
import {
  Panel,
  Label
} from 'react-bootstrap';
import { provideHooks } from 'redial';

const styles = require('./Profile.scss');

@provideHooks({
  fetch: async ({ store: { dispatch, getState }, params: { user_id }, cookies }) => {
    if (!getState().profile.profile.loaded && cookies) {
      await dispatch(loadProfile(cookies.mendoan, user_id)).catch(() => null);
    }
  }
})

@connect(state => ({
  user: state.auth.user,
  profile: state.profile.profile
}), {
  loadProfile
})

export default class Profile extends Component {
  componentWillMount() {
    const {
      user, loadProfile, match, profile
    } = this.props;

    const userId = match.params.user_id;

    if (!profile.loaded || profile.data.hash_id !== userId) {
      loadProfile(user.token, userId);
    }
  }

  render() {
    const { profile } = this.props;
    // require the logo image both from client and server
    return (
      <div>
        <Helmet title={`${profile.data.full_name}`} />
        <MyNavbar />
        <div className="container top-container">
          <div className="row">
            <div className="col-lg-6 col-lg-offset-3">
              <DynamicProfileCard />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
