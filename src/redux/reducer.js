import multireducer from 'multireducer';
import { routerReducer } from 'react-router-redux';
import { reducer as form } from 'redux-form';
import auth from './modules/auth';
import notifs from './modules/notifs';
import counter from './modules/counter';
import info from './modules/info';
import blog from './modules/blog';
import newsfeed from './modules/newsfeed';
import domainAvailability from './modules/domainAvailability';
import subscription from './modules/subscription';
import invoice from './modules/invoice';
import profile from './modules/profile';
import order from './modules/order';

import record from './modules/registrar/record';
import domain from './modules/registrar/domain';
import nameserver from './modules/registrar/nameserver';

export default function createReducers(asyncReducers) {
  return {
    router: routerReducer,
    online: (v = true) => v,
    form,
    notifs,
    auth,
    counter: multireducer({
      counter1: counter,
      counter2: counter,
      counter3: counter
    }),
    info,
    blog,
    newsfeed,
    subscription,
    domainAvailability,
    invoice,
    record,
    domain,
    nameserver,
    profile,
    order,
    ...asyncReducers
  };
}
