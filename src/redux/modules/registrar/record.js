import config from './../../../config';

const LOAD_CREATE = 'record/create/LOAD';
const LOAD_CREATE_SUCCESS = 'record/create/LOAD_SUCCESS';
const LOAD_CREATE_FAIL = 'record/create/LOAD_FAIL';

const LOAD_LIST = 'record/list/LOAD';
const LOAD_LIST_SUCCESS = 'record/list/LOAD_SUCCESS';
const LOAD_LIST_FAIL = 'record/list/LOAD_FAIL';

const LOAD_DELETE = 'record/delete/LOAD';
const LOAD_DELETE_SUCCESS = 'record/delete/LOAD_SUCCESS';
const LOAD_DELETE_FAIL = 'record/delete/LOAD_FAIL';

const initialState = {
  create: {
    loaded: false,
    data: {}
  },
  list: {
    loaded: false,
    loading: false,
    data: {}
  },
  delete: {
    loaded: false,
    loading: false,
    data: {}
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD_CREATE:
      return {
        ...state,
        create: {
          ...state.create,
          loading: true
        }
      };
    case LOAD_CREATE_SUCCESS:
      return {
        ...state,
        create: {
          ...state.create,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_CREATE_FAIL:
      return {
        ...state,
        create: {
          ...state.create,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case LOAD_LIST:
      return {
        ...state,
        list: {
          ...state.list,
          loading: true,
          loaded: false
        }
      };
    case LOAD_LIST_SUCCESS:
      return {
        ...state,
        list: {
          ...state.list,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_LIST_FAIL:
      return {
        ...state,
        list: {
          ...state.list,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case LOAD_DELETE:
      return {
        ...state,
        delete: {
          ...state.delete,
          loading: true,
          loaded: false
        }
      };
    case LOAD_DELETE_SUCCESS:
      return {
        ...state,
        delete: {
          ...state.delete,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_DELETE_FAIL:
      return {
        ...state,
        delete: {
          ...state.delete,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    default:
      return state;
  }
}

export function create(domainName, data, token) {
  return {
    types: [LOAD_CREATE, LOAD_CREATE_SUCCESS, LOAD_CREATE_FAIL],
    promise: ({ client }) => client.post(`${config.apiUrl}registrar/domain/${domainName}/records/create?token=${token}`, data)
  };
}

export function load(domainName, token) {
  return {
    types: [LOAD_LIST, LOAD_LIST_SUCCESS, LOAD_LIST_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}registrar/domain/${domainName}/records`, {
      headers: {
        Token: token
      }
    })
  };
}

export function deleteRecord(domainName, recordId, token) {
  return {
    types: [LOAD_DELETE, LOAD_DELETE_SUCCESS, LOAD_DELETE_FAIL],
    promise: ({ client }) =>
      client.post(`${config.apiUrl}registrar/domain/${domainName}/records/${recordId}/delete`, {}, {
        headers: {
          Token: token
        }
      })
  };
}
