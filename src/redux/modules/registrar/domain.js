import config from './../../../config';

const LOAD_PRODUCT = 'domain/product/LOAD';
const LOAD_PRODUCT_SUCCESS = 'domain/product/LOAD_SUCCESS';
const LOAD_PRODUCT_FAIL = 'domain/product/LOAD_FAIL';

const DOMAIN_SEARCH_FORM_UPDATE = 'domain/search/FORM_UPDATE';
const DOMAIN_BUY_FORM_UPDATE = 'domain/buy/FORM_UPDATE';

const DOMAIN_BOOKED_UPDATE = 'domain/booked/UPDATE';

const LOAD_BOOK = 'domain/book/LOAD';
const LOAD_BOOK_SUCCESS = 'domain/book/LOAD_SUCCESS';
const LOAD_BOOK_FAIL = 'domain/book/LOAD_FAIL';

const LOAD_MY_DOMAIN = 'domain/my/LOAD';
const LOAD_MY_DOMAIN_SUCCESS = 'domain/my/LOAD_SUCCESS';
const LOAD_MY_DOMAIN_FAIL = 'domain/my/LOAD_FAIL';

const LOAD_SEARCH_DOMAIN = 'domain/search/LOAD';
const LOAD_SEARCH_DOMAIN_SUCCESS = 'domain/search/LOAD_SUCCESS';
const LOAD_SEARCH_DOMAIN_FAIL = 'domain/search/LOAD_FAIL';

const LOAD_DOMAIN_DETAIL = 'domain/detail/LOAD';
const LOAD_DOMAIN_DETAIL_SUCCESS = 'domain/detail/LOAD_SUCCESS';
const LOAD_DOMAIN_DETAIL_FAIL = 'domain/detail/LOAD_FAIL';

const FLUSH_SEARCH_DOMAIN = 'domain/search/FLUSH';

const initialState = {
  loaded: false,
  data: {},
  products: {
    loaded: false,
    data: {}
  },
  domain_search_form: {
    domain_name: '',
    extension: '.id'
  },
  domain_search: {
    loaded: false,
    data: {}
  },
  domain_booked: {},
  domain_book: {
    loaded: false,
    loading: false,
    data: {}
  },
  domain_buy_form: {
    product_id: null,
    domain_name: null,
    extension: null,
    name: null,
    state: null,
    city: null,
    email: null,
    phone: null,
    period: 1,
    auto_renew: true,
    bank_code: '014',
    nameservers: [
      'ns1.domain.id',
      'ns2.domain.id'
    ]
  },
  my_domains: {
    loading: false,
    loaded: false
  },
  domain_detail: {
    loading: false,
    loaded: false,
    data: {}
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD_PRODUCT:
      return {
        ...state,
        products: {
          ...state.products,
          loading: true
        }
      };
    case LOAD_PRODUCT_SUCCESS:
      return {
        ...state,
        products: {
          ...state.products,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_PRODUCT_FAIL:
      return {
        ...state,
        products: {
          ...state.products,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case DOMAIN_SEARCH_FORM_UPDATE:
      return {
        ...state,
        domain_search_form: {
          ...state.domain_search_form,
          ...action.data
        }
      };
    case LOAD_SEARCH_DOMAIN:
      return {
        ...state,
        domain_search: {
          ...state.domain_search,
          loading: true
        }
      };
    case LOAD_SEARCH_DOMAIN_SUCCESS:
      return {
        ...state,
        domain_search: {
          ...state.domain_search,
          data: action.result.payload.data,
          loading: false,
          loaded: true,
          error: action.error
        }
      };
    case LOAD_SEARCH_DOMAIN_FAIL:
      return {
        ...state,
        domain_search: {
          ...state.domain_search,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case FLUSH_SEARCH_DOMAIN:
      return {
        ...state,
        domain_search: {
          loaded: false,
          data: {}
        },
        domain_search_form: {
          domain_name: '',
          extension: '.id'
        }
      };
    case DOMAIN_BUY_FORM_UPDATE:
      return {
        ...state,
        domain_buy_form: {
          ...state.domain_buy_form,
          ...action.data
        }
      };
    case LOAD_BOOK:
      return {
        ...state,
        domain_book: {
          ...state.domain_book,
          loading: true
        }
      };
    case LOAD_BOOK_SUCCESS:
      return {
        ...state,
        domain_book: {
          ...state.domain_book,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_BOOK_FAIL:
      return {
        ...state,
        domain_book: {
          ...state.domain_book,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case LOAD_MY_DOMAIN:
      return {
        ...state,
        my_domains: {
          ...state.my_domains,
          loading: true
        }
      };
    case LOAD_MY_DOMAIN_SUCCESS:
      return {
        ...state,
        my_domains: {
          ...state.my_domains,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_MY_DOMAIN_FAIL:
      return {
        ...state,
        my_domains: {
          ...state.my_domains,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case DOMAIN_BOOKED_UPDATE:
      return {
        ...state,
        domain_booked: action.data
      };
    case LOAD_DOMAIN_DETAIL:
      return {
        ...state,
        domain_detail: {
          ...state.domain_detail,
          loading: true,
          loaded: false
        }
      };
    case LOAD_DOMAIN_DETAIL_SUCCESS:
      return {
        ...state,
        domain_detail: {
          ...state.domain_detail,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_DOMAIN_DETAIL_FAIL:
      return {
        ...state,
        domain_detail: {
          ...state.domain_detail,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    default:
      return state;
  }
}

export function loadProduct() {
  return {
    types: [LOAD_PRODUCT, LOAD_PRODUCT_SUCCESS, LOAD_PRODUCT_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}registrar/domain/products`)
  };
}

export function bookDomain(data, token) {
  return {
    types: [LOAD_BOOK, LOAD_BOOK_SUCCESS, LOAD_BOOK_FAIL],
    promise: ({ client }) => client.post(`${config.apiUrl}registrar/domain/book?token=${token}`, data)
  };
}

export function isProductLoaded(globalState) {
  return globalState.domain.products.loaded;
}

export function updateDomainSearchForm(e) {
  const data = {
    [e.target.name]: e.target.value
  };
  return { type: DOMAIN_SEARCH_FORM_UPDATE, data };
}

export function updateDomainBuyForm(e) {
  const data = {
    [e.target.name]: e.target.value
  };
  return { type: DOMAIN_BUY_FORM_UPDATE, data };
}

export function appendDomainBuyFormData(data) {
  return { type: DOMAIN_BUY_FORM_UPDATE, data };
}

export function searchDomain(domainName) {
  return {
    types: [LOAD_SEARCH_DOMAIN, LOAD_SEARCH_DOMAIN_SUCCESS, LOAD_SEARCH_DOMAIN_FAIL],
    promise: ({ client }) => client.post(`${config.apiUrl}registrar/domain/find`, {
      domain_name: domainName
    })
  };
}

export function loadMyDomain(token) {
  return {
    types: [LOAD_MY_DOMAIN, LOAD_MY_DOMAIN_SUCCESS, LOAD_MY_DOMAIN_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}registrar/mydomain`, {
      headers: {
        Token: token
      }
    })
  };
}

export function loadDomainDetail(token, domainName) {
  return {
    types: [LOAD_DOMAIN_DETAIL, LOAD_DOMAIN_DETAIL_SUCCESS, LOAD_DOMAIN_DETAIL_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}registrar/domain/${domainName}`, {
      headers: {
        Token: token
      }
    })
  };
}

export function changeDomainBooked(data) {
  return { type: DOMAIN_BOOKED_UPDATE, data };
}

export function flushSearchData() {
  return { type: FLUSH_SEARCH_DOMAIN };
}
