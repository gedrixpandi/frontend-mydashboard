import config from './../../../config';

const LOAD_UPDATE = 'nameserver/update/LOAD';
const LOAD_UPDATE_SUCCESS = 'nameserver/update/LOAD_SUCCESS';
const LOAD_UPDATE_FAIL = 'nameserver/update/LOAD_FAIL';

const initialState = {
  update: {
    loaded: false,
    loading: false,
    data: {}
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD_UPDATE:
      return {
        ...state,
        update: {
          ...state.update,
          loading: true,
          loaded: false
        }
      };
    case LOAD_UPDATE_SUCCESS:
      return {
        ...state,
        update: {
          ...state.update,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_UPDATE_FAIL:
      return {
        ...state,
        update: {
          ...state.update,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    default:
      return state;
  }
}

export function update(domain, hosts, token) {
  return {
    types: [LOAD_UPDATE, LOAD_UPDATE_SUCCESS, LOAD_UPDATE_FAIL],
    promise: ({ client }) => client.post(`${config.apiUrl}registrar/domain/${domain}/nameserver/update?token=${token}`, { nameservers: hosts })
  };
}
