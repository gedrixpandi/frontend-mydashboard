import config from './../../config';

const LOAD = 'invoice/LOAD';
const LOAD_SUCCESS = 'invoice/LOAD_SUCCESS';
const LOAD_FAIL = 'invoice/LOAD_FAIL';

const LOAD_MORE = 'invoice/LOAD_MORE';
const LOAD_MORE_SUCCESS = 'invoice/LOAD_MORE_SUCCESS';
const LOAD_MORE_FAIL = 'invoice/LOAD_MORE_FAIL';

const LOAD_DETAIL = 'invoice/detail/LOAD';
const LOAD_DETAIL_SUCCESS = 'invoice/detail/LOAD_SUCCESS';
const LOAD_DETAIL_FAIL = 'invoice/detail/LOAD_FAIL';

const LOAD_PAY = 'invoice/pay/LOAD';
const LOAD_PAY_SUCCESS = 'invoice/pay/LOAD_SUCCESS';
const LOAD_PAY_FAIL = 'invoice/pay/LOAD_FAIL';

const LOAD_COUNT = 'invoice/count/LOAD';
const LOAD_COUNT_SUCCESS = 'invoice/count/LOAD_SUCCESS';
const LOAD_COUNT_FAIL = 'invoice/count/LOAD_FAIL';

const initialState = {
  loaded: false,
  data: [],
  detail: {
    data: {},
    loaded: false
  },
  count: {
    data: {},
    loaded: false
  },
  pay: {
    data: {},
    loaded: false
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        unpaid_count: action.result.payload.unpaid_count,
        data: action.result.payload.data.data,
        next_page_url: action.result.payload.data.next_page_url
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case LOAD_MORE:
      return {
        ...state,
        loading: true
      };
    case LOAD_MORE_SUCCESS:
      return {
        ...state,
        loading: false,
        data: state.data.concat(action.result.payload.data.data),
        next_page_url: action.result.payload.data.next_page_url
      };
    case LOAD_MORE_FAIL:
      return {
        ...state,
        loading: false,
        error: action.error
      };
    case LOAD_DETAIL:
      return {
        ...state,
        detail: {
          ...state.detail,
          loading: true
        }
      };
    case LOAD_DETAIL_SUCCESS:
      return {
        ...state,
        detail: {
          ...state.detail,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_DETAIL_FAIL:
      return {
        ...state,
        detail: {
          ...state.detail,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case LOAD_PAY:
      return {
        ...state,
        pay: {
          ...state.pay,
          loading: true
        }
      };
    case LOAD_PAY_SUCCESS:
      return {
        ...state,
        pay: {
          ...state.pay,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_PAY_FAIL:
      return {
        ...state,
        pay: {
          ...state.pay,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case LOAD_COUNT:
      return {
        ...state,
        count: {
          ...state.count,
          loading: true
        }
      };
    case LOAD_COUNT_SUCCESS:
      return {
        ...state,
        count: {
          ...state.count,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_COUNT_FAIL:
      return {
        ...state,
        count: {
          ...state.count,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.invoice && globalState.invoice.loaded;
}

export function load(token) {
  const url = `${config.apiUrl}invoices`;
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: ({ client }) => client.get(url, {
      headers: {
        Token: token
      }
    })
  };
}

export function loadDetail(id, token) {
  return {
    types: [LOAD_DETAIL, LOAD_DETAIL_SUCCESS, LOAD_DETAIL_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}invoices/${id}`, {
      headers: {
        Token: token
      }
    })
  };
}

export function pay(id, token) {
  return {
    types: [LOAD_PAY, LOAD_PAY_SUCCESS, LOAD_PAY_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}invoices/${id}/pay`, {
      headers: {
        Token: token
      }
    })
  };
}

export function count(token) {
  return {
    types: [LOAD_COUNT, LOAD_COUNT_SUCCESS, LOAD_COUNT_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}invoices/unpaid-count`, {
      headers: {
        Token: token
      }
    })
  };
}

export function loadMore(invoice, token) {
  return {
    types: [LOAD_MORE, LOAD_MORE_SUCCESS, LOAD_MORE_FAIL],
    promise: ({ client }) => client.get(invoice.next_page_url, {
      headers: {
        Token: token
      }
    })
  };
}
