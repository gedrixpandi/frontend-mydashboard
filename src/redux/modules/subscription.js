import config from './../../config';

const LOAD = 'subscription/LOAD';
const LOAD_SUCCESS = 'subscription/LOAD_SUCCESS';
const LOAD_FAIL = 'subscription/LOAD_FAIL';

const LOAD_PACKAGE = 'subscription/package/LOAD';
const LOAD_PACKAGE_SUCCESS = 'subscription/package/LOAD_SUCCESS';
const LOAD_PACKAGE_FAIL = 'subscription/package/LOAD_FAIL';

const LOAD_RENEW = 'subscription/renew/LOAD';
const LOAD_RENEW_SUCCESS = 'subscription/renew/LOAD_SUCCESS';
const LOAD_RENEW_FAIL = 'subscription/renew/LOAD_FAIL';

const initialState = {
  loaded: false,
  data: null,
  package: {},
  renew: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.payload.data
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case LOAD_PACKAGE:
      return {
        ...state,
        package: {
          ...state.package,
          loading: true
        }
      };
    case LOAD_PACKAGE_SUCCESS:
      return {
        ...state,
        package: {
          ...state.package,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_PACKAGE_FAIL:
      return {
        ...state,
        package: {
          ...state.package,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case LOAD_RENEW:
      return {
        ...state,
        renew: {
          ...state.renew,
          loading: true
        }
      };
    case LOAD_RENEW_SUCCESS:
      return {
        ...state,
        renew: {
          ...state.renew,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_RENEW_FAIL:
      return {
        ...state,
        renew: {
          ...state.renew,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.subscription && globalState.subscription.loaded;
}

export function load(token) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}subscriptions/status?token=${token}`)
  };
}

export function loadPackage(token) {
  return {
    types: [LOAD_PACKAGE, LOAD_PACKAGE_SUCCESS, LOAD_PACKAGE_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}subscriptions/package?token=${token}`)
  };
}

export function renew(data, token) {
  return {
    types: [LOAD_RENEW, LOAD_RENEW_SUCCESS, LOAD_RENEW_FAIL],
    promise: ({ client }) => client.post(`${config.apiUrl}subscriptions/renew?token=${token}`, data)
  };
}
