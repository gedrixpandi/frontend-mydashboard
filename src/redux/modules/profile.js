import config from './../../config';

const LOAD = 'profile/LOAD';
const LOAD_SUCCESS = 'profile/LOAD_SUCCESS';
const LOAD_FAIL = 'profile/LOAD_FAIL';

const LOAD_FOLLOW = 'profile/follow/LOAD';
const LOAD_FOLLOW_SUCCESS = 'profile/follow/LOAD_SUCCESS';
const LOAD_FOLLOW_FAIL = 'profile/follow/LOAD_FAIL';

const LOAD_UNFOLLOW = 'profile/unfollow/LOAD';
const LOAD_UNFOLLOW_SUCCESS = 'profile/unfollow/LOAD_SUCCESS';
const LOAD_UNFOLLOW_FAIL = 'profile/unfollow/LOAD_FAIL';

const LOAD_UID_PROFILE = 'profile/uid/LOAD';
const LOAD_UID_PROFILE_SUCCESS = 'profile/uid/LOAD_SUCCESS';
const LOAD_UID_PROFILE_FAIL = 'profile/uid/LOAD_FAIL';

const SWITCH_FOLLOWED_STATUS = 'profile/SWITCH_FOLLOWED_STATUS';

const initialState = {
  profile: {
    loaded: false,
    data: {}
  },
  follow: {
    loaded: false,
    data: {}
  },
  unfollow: {
    loaded: false,
    data: {}
  },
  uid_profile: {
    loaded: false,
    data: {}
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        profile: {
          ...state.profile,
          loading: true
        }
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        profile: {
          ...state.profile,
          loading: false,
          loaded: true,
          data: action.result.payload.data || null
        }
      };
    case LOAD_FAIL:
      return {
        ...state,
        profile: {
          ...state.profile,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case LOAD_FOLLOW:
      return {
        ...state,
        follow: {
          ...state.follow,
          loading: true
        }
      };
    case LOAD_FOLLOW_SUCCESS:
      return {
        ...state,
        follow: {
          ...state.follow,
          loading: false,
          loaded: true,
          data: action.result.payload.data || null
        }
      };
    case LOAD_FOLLOW_FAIL:
      return {
        ...state,
        follow: {
          ...state.follow,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case LOAD_UNFOLLOW:
      return {
        ...state,
        unfollow: {
          ...state.unfollow,
          loading: true
        }
      };
    case LOAD_UNFOLLOW_SUCCESS:
      return {
        ...state,
        unfollow: {
          ...state.unfollow,
          loading: false,
          loaded: true,
          data: action.result.payload.data || null
        }
      };
    case LOAD_UNFOLLOW_FAIL:
      return {
        ...state,
        unfollow: {
          ...state.unfollow,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case SWITCH_FOLLOWED_STATUS:
      return {
        ...state,
        profile: {
          ...state.profile,
          data: {
            ...state.profile.data,
            is_followed: !state.profile.data.is_followed,
            followers: state.profile.data.is_followed ? state.profile.data.followers - 1 : state.profile.data.followers + 1
          }
        }
      };
    case LOAD_UID_PROFILE:
      return {
        ...state,
        uid_profile: {
          ...state.uid_profile,
          loading: true
        }
      };
    case LOAD_UID_PROFILE_SUCCESS:
      return {
        ...state,
        uid_profile: {
          ...state.uid_profile,
          loading: false,
          loaded: true,
          data: action.result.payload.data || null
        }
      };
    case LOAD_UID_PROFILE_FAIL:
      return {
        ...state,
        uid_profile: {
          ...state.uid_profile,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    default:
      return state;
  }
}

export function loadProfile(token, user_id) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}profile/${user_id}?token=${token}`)
  };
}

export function loadUidProfile(token) {
  return {
    types: [LOAD_UID_PROFILE, LOAD_UID_PROFILE_SUCCESS, LOAD_UID_PROFILE_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}uid-user-detail`, {
      headers: {
        token
      }
    })
  };
}

export function follow(token, user_id) {
  return {
    types: [LOAD_FOLLOW, LOAD_FOLLOW_SUCCESS, LOAD_FOLLOW_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}profile/${user_id}/follow?token=${token}`)
  };
}

export function unfollow(token, user_id) {
  return {
    types: [LOAD_UNFOLLOW, LOAD_UNFOLLOW_SUCCESS, LOAD_UNFOLLOW_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}profile/${user_id}/unfollow?token=${token}`)
  };
}

export function switchFollowedStatus() {
  return {
    type: SWITCH_FOLLOWED_STATUS
  };
}
