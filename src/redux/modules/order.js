import config from './../../config';

const LOAD = 'order/LOAD';
const LOAD_SUCCESS = 'order/LOAD_SUCCESS';
const LOAD_FAIL = 'order/LOAD_FAIL';

const LOAD_CHECKOUT = 'order/checkout/LOAD';
const LOAD_CHECKOUT_SUCCESS = 'order/checkout/LOAD_SUCCESS';
const LOAD_CHECKOUT_FAIL = 'order/checkout/LOAD_FAIL';

const LOAD_ADD_ITEM = 'order/item/add/LOAD';
const LOAD_ADD_ITEM_SUCCESS = 'order/item/add/LOAD_SUCCESS';
const LOAD_ADD_ITEM_FAIL = 'order/item/add/LOAD_FAIL';

const LOAD_DELETE_ITEM = 'order/item/delete/LOAD';
const LOAD_DELETE_ITEM_SUCCESS = 'order/item/delete/LOAD_SUCCESS';
const LOAD_DELETE_ITEM_FAIL = 'order/item/delete/LOAD_FAIL';

const initialState = {
  active: {
    loading: false,
    loaded: false
  },
  checkout: {
    loading: false,
    loaded: false
  },
  item_added: {
    loading: false,
    loaded: false
  },
  item_deleted: {
    loading: false,
    loaded: false
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        active: {
          ...state.active,
          loading: true
        }
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        active: {
          ...state.active,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_FAIL:
      return {
        ...state,
        active: {
          ...state.active,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case LOAD_CHECKOUT:
      return {
        ...state,
        checkout: {
          ...state.checkout,
          loading: true
        }
      };
    case LOAD_CHECKOUT_SUCCESS:
      return {
        ...state,
        checkout: {
          ...state.checkout,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_CHECKOUT_FAIL:
      return {
        ...state,
        checkout: {
          ...state.checkout,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case LOAD_ADD_ITEM:
      return {
        ...state,
        item_added: {
          ...state.item_added,
          loading: true
        }
      };
    case LOAD_ADD_ITEM_SUCCESS:
      return {
        ...state,
        item_added: {
          ...state.item_added,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_ADD_ITEM_FAIL:
      return {
        ...state,
        item_added: {
          ...state.item_added,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case LOAD_DELETE_ITEM:
      return {
        ...state,
        item_deleted: {
          ...state.item_deleted,
          loading: true
        }
      };
    case LOAD_DELETE_ITEM_SUCCESS:
      return {
        ...state,
        item_deleted: {
          ...state.item_deleted,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_DELETE_ITEM_FAIL:
      return {
        ...state,
        item_deleted: {
          ...state.item_deleted,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.order.active && globalState.order.active.loaded;
}

export function load(token) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: ({ client }) =>
      client.get(`${config.apiUrl}order`, {
        headers: {
          Token: token
        }
      })
  };
}

export function addItem(data, token) {
  return {
    types: [LOAD_ADD_ITEM, LOAD_ADD_ITEM_SUCCESS, LOAD_ADD_ITEM_FAIL],
    promise: ({ client }) =>
      client.put(`${config.apiUrl}order/item/add`, data, {
        headers: {
          Token: token
        }
      })
  };
}

export function deleteItem(data, token) {
  return {
    types: [LOAD_DELETE_ITEM, LOAD_DELETE_ITEM_SUCCESS, LOAD_DELETE_ITEM_FAIL],
    promise: ({ client }) =>
      client.put(`${config.apiUrl}order/item/delete`, data, {
        headers: {
          Token: token
        }
      })
  };
}

export function checkout(data, token) {
  return {
    types: [LOAD_CHECKOUT, LOAD_CHECKOUT_SUCCESS, LOAD_CHECKOUT_FAIL],
    promise: ({ client }) =>
      client.post(`${config.apiUrl}order/checkout`, data, {
        headers: {
          Token: token
        }
      })
  };
}
