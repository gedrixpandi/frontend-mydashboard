import config from './../../config';

const LOAD = 'newsfeed/LOAD';
const LOAD_SUCCESS = 'newsfeed/LOAD_SUCCESS';
const LOAD_FAIL = 'newsfeed/LOAD_FAIL';

const LOAD_MORE = 'newsfeed/LOAD_MORE';
const LOAD_MORE_SUCCESS = 'newsfeed/LOAD_MORE_SUCCESS';
const LOAD_MORE_FAIL = 'newsfeed/LOAD_MORE_FAIL';

const initialState = {
  loaded: false,
  data: null
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true,
        loaded: false
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        meta: action.result.payload.meta || null,
        data: action.result.payload.data || null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case LOAD_MORE:
      return {
        ...state,
        loading: true
      };
    case LOAD_MORE_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        meta: action.result.payload.meta,
        data: state.data.concat(action.result.payload.data)
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.newsfeed && globalState.newsfeed.loaded;
}

export function load(token) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}blogs/me/feeds?limit=10&offset=0&token=${token}`)
  };
}

export function loadFollowing(token) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}blogs/me/feeds/following?limit=10&offset=0&token=${token}`)
  };
}

export function loadMore(url) {
  return {
    types: [LOAD_MORE, LOAD_MORE_SUCCESS, LOAD_MORE_FAIL],
    promise: ({ client }) => client.get(url)
  };
}
