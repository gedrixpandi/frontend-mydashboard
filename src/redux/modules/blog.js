import config from './../../config';

const LOAD = 'blog/LOAD';
const LOAD_SUCCESS = 'blog/LOAD_SUCCESS';
const LOAD_FAIL = 'blog/LOAD_FAIL';
const RESET = 'blog/RESET';

const LOAD_DETAIL = 'blog/detail/LOAD';
const LOAD_DETAIL_SUCCESS = 'blog/detail/LOAD_SUCCESS';
const LOAD_DETAIL_FAIL = 'blog/detail/LOAD_FAIL';

const LOAD_CREATE = 'blog/create/LOAD';
const LOAD_CREATE_SUCCESS = 'blog/create/LOAD_SUCCESS';
const LOAD_CREATE_FAIL = 'blog/create/LOAD_FAIL';

const LOAD_CB = 'blog/callback/LOAD';
const LOAD_CB_SUCCESS = 'blog/callback/LOAD_SUCCESS';
const LOAD_CB_FAIL = 'blog/callback/LOAD_FAIL';

const LOAD_MX = 'blog/mx/LOAD';
const LOAD_MX_SUCCESS = 'blog/mx/LOAD_SUCCESS';
const LOAD_MX_FAIL = 'blog/mx/LOAD_FAIL';

const LOAD_MX_DELETE = 'blog/mx/delete/LOAD';
const LOAD_MX_DELETE_SUCCESS = 'blog/mx/delete/LOAD_SUCCESS';
const LOAD_MX_DELETE_FAIL = 'blog/mx/delete/LOAD_FAIL';

const LOAD_CNAME_CHECK = 'blog/cname/check/LOAD';
const LOAD_CNAME_CHECK_SUCCESS = 'blog/cname/check/LOAD_SUCCESS';
const LOAD_CNAME_CHECK_FAIL = 'blog/cname/check/LOAD_FAIL';
const RESET_CNAME_CHECK = 'blog/cname/check/RESET';

const LOAD_ALIAS_AVAILABILITY = 'blog/alias/availability/LOAD';
const LOAD_ALIAS_AVAILABILITY_SUCCESS = 'blog/alias/availability/LOAD_SUCCESS';
const LOAD_ALIAS_AVAILABILITY_FAIL = 'blog/alias/availability/LOAD_FAIL';
const RESET_ALIAS_AVAILABILITY = 'blog/alias/availability/RESET';

const LOAD_CUSTOMIZE_DOMAIN = 'blog/domain/customize/LOAD';
const LOAD_CUSTOMIZE_DOMAIN_SUCCESS = 'blog/domain/customize/LOAD_SUCCESS';
const LOAD_CUSTOMIZE_DOMAIN_FAIL = 'blog/domain/customize/LOAD_FAIL';

const LOAD_DOMAIN_PRODUCT = 'blog/domain/product/LOAD';
const LOAD_DOMAIN_PRODUCT_SUCCESS = 'blog/domain/product/LOAD_SUCCESS';
const LOAD_DOMAIN_PRODUCT_FAIL = 'blog/domain/product/LOAD_FAIL';

const initialState = {
  loaded: false,
  create_loaded: false,
  data: {},
  detail: {
    loaded: false,
    data: {}
  },
  callback: {
    loaded: false,
    data: {}
  },
  mx: {
    loaded: false,
    data: {}
  },
  cname_check: {
    loaded: false,
    data: {}
  },
  alias_availability: {
    loaded: false,
    data: {}
  },
  customize_domain: {
    loaded: false,
    data: {}
  },
  domainProducts: {
    loaded: false,
    data: {}
  }
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        data: action.result.payload.data || null
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case LOAD_CREATE:
      return {
        ...state,
        create_loading: true
      };
    case LOAD_CREATE_SUCCESS:
      return {
        ...state,
        create_loading: false,
        create_loaded: true,
        create_success: action.result.status === 'success',
        create_errors: action.result.status === 'failed' ? action.result.payload.errors : null,
      };
    case LOAD_CREATE_FAIL:
      return {
        ...state,
        create_loading: false,
        create_loaded: false,
        create_error: action.error
      };
    case LOAD_DETAIL:
      return {
        ...state,
        detail: {
          ...state.detail,
          loading: true
        }
      };
    case LOAD_DETAIL_SUCCESS:
      return {
        ...state,
        detail: {
          ...state.detail,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_DETAIL_FAIL:
      return {
        ...state,
        detail: {
          ...state.detail,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case LOAD_CB:
      return {
        ...state,
        callback: {
          ...state.callback,
          loading: true
        }
      };
    case LOAD_CB_SUCCESS:
      return {
        ...state,
        callback: {
          ...state.callback,
          loading: false,
          loaded: true,
          status: action.result.status
        }
      };
    case LOAD_CB_FAIL:
      return {
        ...state,
        callback: {
          ...state.callback,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case LOAD_MX:
      return {
        ...state,
        mx: {
          ...state.mx,
          loading: true
        }
      };
    case LOAD_MX_SUCCESS:
      return {
        ...state,
        mx: {
          ...state.mx,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_MX_FAIL:
      return {
        ...state,
        mx: {
          ...state.mx,
          loading: false,
          loaded: false,
          error: action.error
        }
      };

    case LOAD_CNAME_CHECK:
      return {
        ...state,
        cname_check: {
          ...state.cname_check,
          loading: true
        }
      };
    case LOAD_CNAME_CHECK_SUCCESS:
      return {
        ...state,
        cname_check: {
          ...state.cname_check,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_CNAME_CHECK_FAIL:
      return {
        ...state,
        cname_check: {
          ...state.cname_check,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case RESET_CNAME_CHECK:
      return {
        ...state,
        cname_check: {
          loaded: false,
          data: {}
        }
      };

    case LOAD_ALIAS_AVAILABILITY:
      return {
        ...state,
        alias_availability: {
          ...state.alias_availability,
          loading: true
        }
      };
    case LOAD_ALIAS_AVAILABILITY_SUCCESS:
      return {
        ...state,
        alias_availability: {
          ...state.alias_availability,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_ALIAS_AVAILABILITY_FAIL:
      return {
        ...state,
        alias_availability: {
          ...state.alias_availability,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case RESET_ALIAS_AVAILABILITY:
      return {
        ...state,
        alias_availability: {
          loaded: false,
          data: {}
        }
      };

    case LOAD_CUSTOMIZE_DOMAIN:
      return {
        ...state,
        customize_domain: {
          ...state.customize_domain,
          loading: true
        }
      };
    case LOAD_CUSTOMIZE_DOMAIN_SUCCESS:
      return {
        ...state,
        customize_domain: {
          ...state.customize_domain,
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_CUSTOMIZE_DOMAIN_FAIL:
      return {
        ...state,
        customize_domain: {
          ...state.customize_domain,
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case LOAD_DOMAIN_PRODUCT:
      return {
        ...state,
        domainProducts: {
          loading: true,
          loaded: false
        }
      };
    case LOAD_DOMAIN_PRODUCT_SUCCESS:
      return {
        ...state,
        domainProducts: {
          loading: false,
          loaded: true,
          data: action.result.payload.data
        }
      };
    case LOAD_DOMAIN_PRODUCT_FAIL:
      return {
        ...state,
        domainProducts: {
          loading: false,
          loaded: false,
          error: action.error
        }
      };
    case RESET:
      return {
        ...state,
        create_loaded: false,
      };
    default:
      return state;
  }
}

export function isLoaded(globalState) {
  return globalState.blog && globalState.blog.loaded;
}

export function load(token) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}blogs/me?token=${token}`)
  };
}

export function create(data, token) {
  return {
    types: [LOAD_CREATE, LOAD_CREATE_SUCCESS, LOAD_CREATE_FAIL],
    promise: ({ client }) => client.post(`${config.apiUrl}blogs/create?token=${token}`, data)
  };
}

export function loadDetail(domain, token) {
  return {
    types: [LOAD_DETAIL, LOAD_DETAIL_SUCCESS, LOAD_DETAIL_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}blogs/${domain}?token=${token}`)
  };
}

export function authBlog(domain, access_token) {
  return {
    types: [LOAD_CB, LOAD_CB_SUCCESS, LOAD_CB_FAIL],
    promise: ({ client }) => client.get(`http://${domain}/cb.php?access_token=${access_token}`)
  };
}

export function loadMx(domain, token) {
  return {
    types: [LOAD_MX, LOAD_MX_SUCCESS, LOAD_MX_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}blogs/${domain}/mx?token=${token}`)
  };
}

export function deleteMx(domain, id, token) {
  return {
    types: [LOAD_MX_DELETE, LOAD_MX_DELETE_SUCCESS, LOAD_MX_DELETE_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}blogs/${domain}/mx/${id}/delete?token=${token}`)
  };
}

export function cnameCheck(domain, customDomain, token) {
  return {
    types: [LOAD_CNAME_CHECK, LOAD_CNAME_CHECK_SUCCESS, LOAD_CNAME_CHECK_FAIL],
    promise: ({ client }) => client.post(`${config.apiUrl}blogs/${domain}/customize-domain/cname-check?token=${token}`, {
      custom_domain: customDomain
    })
  };
}

export function checkAliasAvailability(domain, token) {
  return {
    types: [LOAD_ALIAS_AVAILABILITY, LOAD_ALIAS_AVAILABILITY_SUCCESS, LOAD_ALIAS_AVAILABILITY_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}blogs/alias-availability?domain=${domain}&token=${token}`)
  };
}

export function customizeDomain(domain, customDomain, token) {
  return {
    types: [LOAD_CUSTOMIZE_DOMAIN, LOAD_CUSTOMIZE_DOMAIN_SUCCESS, LOAD_CUSTOMIZE_DOMAIN_FAIL],
    promise: ({ client }) => client.post(`${config.apiUrl}blogs/${domain}/customize-domain?token=${token}`, {
      custom_domain: customDomain
    })
  };
}

export function loadDomainProducts() {
  return {
    types: [LOAD_DOMAIN_PRODUCT, LOAD_DOMAIN_PRODUCT_SUCCESS, LOAD_DOMAIN_PRODUCT_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}registrar/domain/products/bundling`)
  };
}

export function resetAliasAvailability() {
  return {
    type: RESET_ALIAS_AVAILABILITY
  };
}

export function resetCnameCheck() {
  return {
    type: RESET_CNAME_CHECK
  };
}

export function resetData() {
  return {
    type: RESET
  };
}
