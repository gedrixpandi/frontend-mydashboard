import config from './../../config';

const LOAD = 'domain/availability/LOAD';
const LOAD_SUCCESS = 'domain/availability/LOAD_SUCCESS';
const LOAD_FAIL = 'domain/availability/LOAD_FAIL';
const RESET = 'domain/availability/RESET';

const initialState = {
  loaded: false
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case LOAD:
      return {
        ...state,
        loading: true,
        checked: false
      };
    case LOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        loaded: true,
        checked: true,
        name: action.result.payload.data.name,
        is_available: action.result.payload.data.is_available
      };
    case LOAD_FAIL:
      return {
        ...state,
        loading: false,
        loaded: false,
        error: action.error
      };
    case RESET:
      return {
        loaded: false
      };
    default:
      return state;
  }
}

export function checkDomain(token, name) {
  return {
    types: [LOAD, LOAD_SUCCESS, LOAD_FAIL],
    promise: ({ client }) => client.get(`${config.apiUrl}domains/check?name=${name}&token=${token}`)
  };
}

export function resetData() {
  return {
    type: RESET
  };
}
