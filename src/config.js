const environment = {
  development: {
    isProduction: false,
    assetsPath: `http://${process.env.HOST || 'localhost'}:${+process.env.PORT + 1 || 3001}/dist/`
  },
  production: {
    isProduction: true,
    assetsPath: '/dist/'
  }
}[process.env.NODE_ENV || 'development'];

const uid = {
  uid_url: 'https://api.u.id/',
  uid_client_id: process.env.UID_CLIENT,
  uid_secret: process.env.UID_SECRET,
  uid_login: process.env.UID_LOGIN,
  uid_redirect_uri: process.env.UID_REDIRECT_URI
};

module.exports = Object.assign(
  {
    host: process.env.HOST || 'localhost',
    port: process.env.PORT,
    apiHost: process.env.APIHOST || 'localhost',
    apiPort: process.env.APIPORT,
    apiUrl: process.env.APIURL || 'http://apis.my.id/v1/',
    appVersion: process.env.APP_VERSION || '#',
    app: {
      title: 'My.ID - Platform Blog Indonesia',
      description: 'Buat blog dengan domain my.id gratis! Didukung dan dikembangkan oleh PANDI.',
      head: {
        titleTemplate: '%s - My.ID',
        meta: [
          { name: 'description', content: 'Buat blog dengan domain my.id gratis! Didukung dan dikembangkan oleh PANDI.' },
          { charset: 'utf-8' },
          { property: 'og:site_name', content: 'My.ID' },
          { property: 'og:image', content: 'https://www.my.id/my_og.jpg' },
          { property: 'og:locale', content: 'id_ID' },
          { property: 'og:title', content: 'My.ID' },
          { property: 'og:description', content: 'Buat blog dengan domain my.id gratis! Didukung dan dikembangkan oleh PANDI.' },
          { property: 'og:card', content: 'summary' }
        ]
      }
    }
  },
  environment,
  uid
);
