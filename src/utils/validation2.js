const isEmpty = value => value === undefined || value === null || value === '';

export function isDomainName(value) {
  if (!isEmpty(value) && !/^[a-zA-Z0-9-]*$/i.test(value)) {
    return false;
  }

  return true;
}

export function isDomainName2(value) {
  if ((!isEmpty(value) && !/^[a-zA-Z0-9-]*$/i.test(value)) || value.length < 5) {
    return false;
  }

  return true;
}
