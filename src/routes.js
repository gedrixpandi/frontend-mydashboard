import { routerActions } from 'react-router-redux';
import { connectedReduxRedirect } from 'redux-auth-wrapper/history4/redirect';
import {
  App,
  Home,
  Subscription,
  Invoices,
  NotFound,
  NewBlog,
  InvoiceDetail,
  ManageBlog,
  BlogMXRecord,
  BlogAuth,
  CustomDomain,
  Registrar,
  Dns,
  DomainSearch,
  Profile,
  EditProfile,
  BuyDomain,
  Cart,
  DomainSettings,
  NewPremiumBlog
} from 'containers';
import Login from 'containers/Login/Loadable';

const isAuthenticated = connectedReduxRedirect({
  redirectPath: '/login',
  authenticatedSelector: state => state.auth.user !== null,
  redirectAction: routerActions.replace,
  wrapperDisplayName: 'UserIsAuthenticated'
});

const isNotAuthenticated = connectedReduxRedirect({
  redirectPath: '/',
  authenticatedSelector: state => state.auth.user === null,
  redirectAction: routerActions.replace,
  wrapperDisplayName: 'UserIsAuthenticated',
  allowRedirectBack: false
});

const isHaveBlog = connectedReduxRedirect({
  redirectPath: '/blogs/new',
  authenticatedSelector: state => state.blog.data.length > 0,
  redirectAction: routerActions.replace,
  wrapperDisplayName: 'UserAlreadyHaveBlog',
  allowRedirectBack: false
});

const isAuthenticatedRegistrar = connectedReduxRedirect({
  redirectPath: '/registrar/domain/search',
  authenticatedSelector: state => state.auth.user !== null,
  redirectAction: routerActions.replace,
  wrapperDisplayName: 'UserIsAuthenticatedRegistrar'
});

const routes = [
  {
    component: App,
    routes: [
      { path: '/', exact: true, component: isAuthenticated(Home) },
      { path: '/subscription', component: isAuthenticated(isHaveBlog(Subscription)) },
      { path: '/invoices/:invoice_id', component: isAuthenticated(InvoiceDetail) },
      { path: '/invoices', component: isAuthenticated(Invoices) },
      { path: '/cart', component: isAuthenticated(Cart) },
      { path: '/profile/edit', component: isAuthenticated(EditProfile) },
      { path: '/profile/:user_id', component: isAuthenticated(Profile) },
      { path: '/blogs/new', exac: true, component: isAuthenticated(NewBlog) },
      { path: '/blogs/new-premium', component: isAuthenticated(NewPremiumBlog) },
      { path: '/blogs/:blog_domain', exact: true, component: isAuthenticated(isHaveBlog(ManageBlog)) },
      { path: '/blogs/:blog_domain/mx', component: isAuthenticated(isHaveBlog(BlogMXRecord)) },
      { path: '/blogs/:blog_domain/admin', component: isAuthenticated(isHaveBlog(BlogAuth)) },
      { path: '/blogs/:blog_domain/domain', component: isAuthenticated(isHaveBlog(CustomDomain)) },
      { path: '/registrar', exact: true, component: isAuthenticatedRegistrar(Registrar) },
      { path: '/registrar/domain/:domain/dns', component: isAuthenticated(Dns) },
      { path: '/registrar/domain/:domain/settings', component: isAuthenticated(DomainSettings) },
      { path: '/registrar/domain/search', exact: true, component: DomainSearch },
      { path: '/registrar/domain/buy', exact: true, component: isAuthenticated(BuyDomain) },
      { path: '/login', component: isNotAuthenticated(Login) },
      { component: NotFound, status: 404 }
    ]
  }
];

export default routes;
